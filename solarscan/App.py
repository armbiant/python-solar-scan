#! /usr/bin/env python

# -*- coding: UTF-8 -*-

# ------------------------------------------------------------------------------
# Project: solarscan ( Solar Scan processing )
#
# This script processes the video solar acquisition of the sun made with the
# instrument Sol'Ex (http://www.astrosurf.com/solex/).
#
# ------------------------------------------------------------------------------
#    Author:  M27trognondepomme <pebe92 (at) gmail.com>
#
# This program is provided without any guarantee.
#
# The license is  LGPL-v3
# For details, see GNU General Public License, version 3 or later.
#                        "https://www.gnu.org/licenses/gpl.html"
# ------------------------------------------------------------------------------

import wx
import os
import sys

# ------------------------------------------------------------------------------
# Check Mode Developement - SolarScan package is not installed
IsPyInstallerBundle= getattr(sys, 'frozen', False) and hasattr(sys, '_MEIPASS')
SolarScanExist="solarscan" in sys.modules
if not SolarScanExist and not IsPyInstallerBundle :
    from pathlib import Path
    file = Path(__file__).resolve()
    try:
        sys.path.remove(str(file.parent))
    except ValueError: # Already removed
        pass
    sys.path.append(str(file.parents[1]))
    print("Info: Mode Developement - SolarScan package is not installed",flush=True)
# ------------------------------------------------------------------------------

from solarscan.lib import tools,callbacks,prefs #@UnresolvedImport
from solarscan.ui  import gui                   #@UnresolvedImport

# ==============================================================================
class App(wx.App):
    def OnInit(self):
        # Configuration
        varname = 'USERPROFILE' if os.name == 'nt' else 'HOME'
        myhome  = os.environ.get(varname)
        i_prefs = prefs.CPrefs( myhome , '.solarscan_rc' )
        i_gui   = gui.CGui(None, -1, "") # instance SolarScan GUI
        callbacks.CCallbacks(i_gui, i_prefs, myhome )

        # add icon
        iconname     = os.path.join(".","icon","solarscan.ico")
        iconpath     = os.path.dirname(os.path.abspath(__file__))
        iconfullname = tools.resource_path(iconname, pathabs=iconpath )
        bitmap       = wx.Bitmap(iconfullname, wx.BITMAP_TYPE_ICO)
        _icon        = wx.NullIcon
        _icon.CopyFromBitmap(bitmap)
        i_gui.SetIcon(_icon)

        i_gui.Show(True)
        self.SetTopWindow(i_gui)
        return True

## -----------------------------------------------------------------------------
def Run():
    if sys.version_info[0] != 3 :
        print( "*")
        print( "* Python version :", sys.version_info )
        print( "* Error, python version should be >= 3 " )
        print( "*")
        exit()
    app = App(0)
    app.MainLoop()

if __name__ == '__main__':
    Run()
