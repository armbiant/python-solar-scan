#! /usr/bin/env python

# -*- coding: UTF-8 -*-

# ------------------------------------------------------------------------------
# Project: solarscan ( Solar Scan processing )
#
# This script processes the video solar acquisition of the sun made with the
# instrument Sol'Ex (http://www.astrosurf.com/solex/).
#
# ------------------------------------------------------------------------------
#    Author:  M27trognondepomme <pebe92 (at) gmail.com>
#
# This program is provided without any guarantee.
#
# The license is  LGPL-v3
# For details, see GNU General Public License, version 3 or later.
#                        "https://www.gnu.org/licenses/gpl.html"
# ------------------------------------------------------------------------------

import os
import sys

# ------------------------------------------------------------------------------
# Check Mode Developement - SolarScan package is not installed
IsPyInstallerBundle= getattr(sys, 'frozen', False) and hasattr(sys, '_MEIPASS')
SolarScanExist="solarscan" in sys.modules
if not SolarScanExist and not IsPyInstallerBundle :
    from pathlib import Path
    file = Path(__file__).resolve()
    try:
        sys.path.remove(str(file.parent))
    except ValueError: # Already removed
        pass
    sys.path.append(str(file.parents[1]))
    print("Info: Mode Developement - SolarScan package is not installed",flush=True)
# ------------------------------------------------------------------------------

from solarscan.lib import changelog,tlog,VideoProcessing,solartools
from solarscan.lib import tools,prefs,cliPlotPanel

# ==============================================================================
class AppCli:
    def __init__(self):
        # Configuration
        varname     = 'USERPROFILE' if os.name == 'nt' else 'HOME'
        self.myhome = os.environ.get(varname)
        self.i_prefs= prefs.CPrefs( self.myhome , '.solarscan_rc' )

    # --------------------------------------------------------------------------
    def Run(self):
        if len(sys.argv) < 2 :
            try:
                import tkinter as tk
                from tkinter import filedialog
                videoname = filedialog.askopenfilename(initialdir="",
                                title="Select Video file",
                                filetypes=(("SER files", "*.ser"),)  )
                if len( videoname ) == 0:
                    print(_("Cancel") + ": " + _("exit") + " " + sys.argv[0])
                    return
            except:
                videoname=input(_('enter video file (SER):'))
        else:
            videoname=sys.argv[1]

        if not os.path.exists( videoname ) :
            print(_("Abort") + ": " + _("video file doesn't exist") + " " +videoname)
            return

        print( _("Video file is:"),videoname)
        
        varname     = 'USERPROFILE' if os.name == 'nt' else 'HOME'
        self.myhome = os.environ.get(varname)
        i_prefs     = prefs.CPrefs( self.myhome , '.solarscan_rc' )

        plot = cliPlotPanel.cliPlotPanel()
        log  = tlog.LogConsole(None, None,None)
        iSolarTools= solartools.solartools(log,plot, i_prefs )
        log.LogTraceMain(True,self.myhome)
        log.LogTraceImage(True,videoname)
        if iSolarTools.OpenSer(videoname) != None :
            iSolarTools.DisplayInfo()
        else:
            print(_("Abort") + ": " + _("video file is bad") + " " +videoname)
            return

        vp = VideoProcessing.VideoProcessing(log,iSolarTools,i_prefs)
        vp.run_all(1)
        plot.pause()

        log.LogTraceMain(False)
        log.LogTraceImage(False)
        input(_('pause: press a key to continue') )

if __name__ == '__main__':
    if sys.version_info[0] != 3 :
        print( "*")
        print( "* Python version :", sys.version_info )
        print( "* Error, python version should be >= 3 " )
        print( "*")
        exit()
    app = AppCli();
    app.Run()
