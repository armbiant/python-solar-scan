#! /usr/bin/env python

# -*- coding: UTF-8 -*-

# ------------------------------------------------------------------------------
# Project: solarscan ( Solar Scan processing )
#
# This script processes the video solar acquisition of the sun made with the
# instrument Sol'Ex (http://www.astrosurf.com/solex/).
#
# ------------------------------------------------------------------------------
#    Author:  M27trognondepomme <pebe92 (at) gmail.com>
#
# This program is provided without any guarantee.
#
# The license is  LGPL-v3
# For details, see GNU General Public License, version 3 or later.
#                        "https://www.gnu.org/licenses/gpl.html"
# ------------------------------------------------------------------------------
import numpy as np
from scipy import ndimage

# ------------------------------------------------------------------------------
def movingaverage(array1D, window, modeconv='full'):
    ''' calculation of the moving average of a 1d array
            parameters:
                array1D= input array to mean
                window= width of the sliding window for the calculation of the average 
            return
                smoothed_array1D= output of smoothed values
    '''
    weights = np.repeat(1.0, window)/window
    smoothed_array1D = np.convolve(array1D, weights, modeconv )
    return smoothed_array1D

# --------------------------------------------------------------------------
def rescale(img,value_max):
    ''' rescale an image to max value '''
    vmin=np.amin(img)
    if vmin > 0 : vmin=0
    return np.copy((img-vmin)*(value_max/np.amax(img)))
   
# --------------------------------------------------------------------------
def img_reinterpoled_xaxis(img, ratio ):
    ''' reinterpole l'image selon l'axe x '''
    ih,iw= img.shape
    newiw=int(iw*ratio)
    rr_out=np.linspace(0.0, 1.0, newiw, endpoint=False)
    rr_in=np.linspace(0.0, 1.0, iw, endpoint=False)
    NewImg=np.zeros((ih,newiw))
    for jj in range(0,ih):
        NewImg[jj,:] = np.interp(rr_out,rr_in,img[jj,:])
    return NewImg

# --------------------------------------------------------------------------
def img_reinterpoled_yaxis(img, ratio ):
    ''' reinterpole l'image selon l'axe y '''
    (ih,iw)= img.shape
    newih=int(ih*ratio) 
    rr_out=np.linspace(0.0, 1.0, newih, endpoint=False)
    rr_in=np.linspace(0.0, 1.0, ih, endpoint=False)
    NewImg=np.zeros((newih,iw))
    for jj in range(0,iw):
        NewImg[:,jj] = np.interp(rr_out,rr_in,img[:,jj])
    return NewImg

# --------------------------------------------------------------------------
def rotateImage(image, angle, pivot_yx):
    dx_left   = pivot_yx[1]
    dx_right  = image.shape[1] - pivot_yx[1]
    dy_top    = pivot_yx[0]
    dy_bottom = image.shape[0] - pivot_yx[0]
    d_max = max((dx_left,dx_right,dy_top,dy_bottom))
    padX = [d_max-dx_left, d_max-dx_right]
    padY = [d_max-dy_top, d_max-dy_bottom]
    imgP = np.pad(image, [padY, padX], 'reflect')
    im_rot =  ndimage.rotate(imgP,angle, reshape=True, mode ='mirror') 
    return im_rot

# --------------------------------------------------------------------------
def rotate_point(yxM, yxO, angle, ratio ) :
    a = np.deg2rad(angle)
    dxM = yxM[1] - yxO[1]
    dyM = yxM[0] - yxO[0]
    dx =   dxM * np.cos(a) + dyM * np.sin(a) 
    dy = - dxM * np.sin(a) + dyM *np.cos(a)
    if ratio <  1: dy *= ratio
    if ratio >= 1: dx /= ratio
    dx2 =   dx * np.cos(-a) + dy * np.sin(-a) 
    dy2 = - dx * np.sin(-a) + dy *np.cos(-a)
    return (int(dy2),int(dx2))

# --------------------------------------------------------------------------
def Eclipse_to_circle(img, ratio, angle_degre, center_yx ):
    imgcpy=np.copy(img)
    if abs(angle_degre) > 0.5 :
        pivot=[int(round(center_yx[0])),int(round(center_yx[1]))]
        imgcpy=rotateImage(imgcpy,-angle_degre,pivot)
        
    if  ratio < 1  :   
        newimg = img_reinterpoled_yaxis(imgcpy, ratio )    
    else: 
        newimg =  img_reinterpoled_xaxis(imgcpy, 1/ratio )
                   
    if abs(angle_degre) > 0.5 :
        newimg = ndimage.rotate(newimg,angle_degre, reshape=False, mode ='mirror')
        n_center=(newimg.shape[0]/2,newimg.shape[1]/2)
        dx=img.shape[1]/2 if  ratio < 1 else img.shape[1]/2/ratio
        dy= ratio*img.shape[0]/2 if  ratio < 1 else img.shape[0]/2
        oy,ox= rotate_point((img.shape[0]/2,img.shape[1]/2) ,pivot,-angle_degre, ratio ) 
        padX0=int(n_center[1]-dx + ox )
        padX1=int(n_center[1]+dx + ox )
        padY0=int(n_center[0]-dy + oy )
        padY1=int(n_center[0]+dy + oy )
        newimg=newimg[padY0:padY1,padX0:padX1]        
    return newimg     

    