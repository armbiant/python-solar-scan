#! /usr/bin/env python

# -*- coding: UTF-8 -*-

# ------------------------------------------------------------------------------
# Project: solarscan ( Solar Scan processing )
#
# This script processes the video solar acquisition of the sun made with the
# instrument Sol'Ex (http://www.astrosurf.com/solex/).
#
# ------------------------------------------------------------------------------
#    Author:  M27trognondepomme <pebe92 (at) gmail.com>
#
# This program is provided without any guarantee.
#
# The license is  LGPL-v3
# For details, see GNU General Public License, version 3 or later.
#                        "https://www.gnu.org/licenses/gpl.html"
# ------------------------------------------------------------------------------
import os
from  math import sqrt,ceil
from solarscan.lib import constantes,tools #@UnresolvedImport
import numpy as np
import cv2
from serfilesreader.serfilesreader import Serfile
from astropy.io import fits

from ellipse import LsqEllipse
from scipy import signal, ndimage
import matplotlib.pyplot as plt
from matplotlib.patches import Ellipse

from solarscan.lib.imgtools import movingaverage, rescale,Eclipse_to_circle  #@UnresolvedImport

class solartools:
    def __init__(self,  log , plot, prefs ):
        self.plot=plot
        self.log=log
        self.videoname=None
        self.serfd=None
        self.prefs=prefs
        
        self.racine=None
        self.racine_out=None
        
        self.data={}
        self.ClrData()
        
    def ClrData(self):
        self.data={}
        self.data["best"]=-1
        self.data["xzoom"]=()
        self.data["yzoom"]=()
        self.data["parabola"]=[]
        self.data["y_mod"]=[]
        
    def LoadData(self):
        self.ClrData()
        if self.videoname is None : 
            return
        if self.racine_out is None : 
            return
        filename=self.racine_out+".dat"

        if not os.path.isfile(filename) : return
        with open(filename,"r", encoding="utf-8") as fd :
            lines= fd.readlines()
            data = tools.String2data(lines)
            try:
                data = tools.String2data(lines)
                for key in self.data.keys():
                    # ne charge que les cles existantes dans self.data()
                    if not ( key in data ):
                        continue # la cle n'existe pas, on passe a la suite
                    self.data[key]=data[key]
            except Exception as e : # pylint: disable=broad-except
                print("*** solartools::Load() error loading "+ filename + " :: " + str(e) )
            
    def SaveData(self):
        if self.videoname is None : 
            return
        if self.racine_out is None : 
            return
        filename=self.racine_out+".dat"
        chaine = tools.Stringify(self.data)
        with open(filename,"w", encoding="utf-8") as fd :
            fd.write("# Don't edit the file \n" )
            fd.write(chaine)
            
    def GetBest(self):
        return self.data["best"]
            
    def GetYmod(self):
        return self.data["y_mod"]

    def OpenSer(self, videoname) :
        log=self.log
        if not os.path.exists(videoname) :
            log.text("*** '"+ videoname +"' " , _("doesn't exist") )
            self.serfd = None
            self.videoname=None
            self.ViewInfo()
            return
            
        try:
            self.serfd = Serfile(videoname, False)
            self.videoname=videoname
            self.racine=os.path.splitext(os.path.basename(self.videoname))[0]
            self.racine_out= os.path.join(os.path.dirname(self.videoname),constantes.PREFIXDIR+self.racine,self.racine)
            
            if self.serfd.getHeader()['ColorID'] > 19 :
                log.text("*** '"+ videoname +"' " , _("is not mono") )
                self.serfd = None
                self.videoname=None
                self.ViewInfo()
                
        except Exception as e: # pylint: disable=broad-except
            log.text("*** Serfile('"+ videoname +"') :" , str( e ) )
            self.serfd = None
            self.videoname=None
            self.ViewInfo()
        return self.serfd
        
    def GetDirOut(self):
        return os.path.dirname(self.racine_out)

    def CloseSer(self) :
        self.serfd = None
        self.videoname=None

    def GetSerFd(self) :
        return self.serfd
    
    def ViewInfo(self,toggle=True):
        self.plot.view_info(toggle)
        
    def ViewImage(self,toggle=False):
        self.plot.view_info(toggle)

    def DisplayInfo(self,videoname=None):
        log=self.log
        if videoname != None :
            self.OpenSer(videoname)
        if self.serfd == None :
            return ""

        serfd = self.serfd

        log.text('')
        log.ligne('.')
        log.titre('.', _("VIDEO LOADED"), ":", self.videoname )
        log.ligne('.')
        log.text('')

        log.ligne('-')
        log.text("o ", _("filename") + ": ", self.videoname )
        log.text("o ", _("frame")    + ": ", serfd.getLength() )
        log.text("o ", _("geometry") + ": ", _("width")   + "=", serfd.getWidth(), " ",
                                             _("height") + "=", serfd.getHeight() )
        log.ligne('-')
        for cle,value in serfd.getHeader().items() :
            log.text("o ", cle,":", value )
        log.ligne('-')
        log.text('')
        
        serfd.setCurrentPosition(int(serfd.getLength()/2))
        image,numero=serfd.read()
        
        splt=self.plot
        splt.clear('all')
        splt.add_subplot()
        splt.imshow(image,cmap='gray' )
        splt.title(_("frame no:")+ str(numero))
        splt.draw()
        
    def GetNumberFrame(self):
        self.ViewInfo()
        if self.videoname != None :
            self.OpenSer(self.videoname)
        if self.serfd == None :
            return -1
        return self.serfd.getLength()

    def CheckNumberFrame(self,no_img ):
        log=self.log
        serfd = self.serfd
        if serfd == None :
            log.text( "**** " + _("No video file opened"))
            self.ViewInfo()
            return True
        if no_img >= serfd.getLength() or no_img < 0 :
            log.text( "**** " + _("bad frame number:"), no_img , " ([0:%d[)"%(serfd.getLength(),))
            self.ViewInfo()
            return True
        return False
    
    def save_png_fit(self,basename,image):      
        cv2.imwrite(basename+ ".png", image.astype('uint16'))
        
        hdr= fits.Header()
        hdr['SIMPLE']='T'
        hdr['BITPIX']=32
        hdr['NAXIS']=2
        hdr['NAXIS1']=image.shape[1]
        hdr['NAXIS2']=image.shape[0]
        hdr['BZERO']=0
        hdr['BSCALE']=1
        hdr['BIN1']=1
        hdr['BIN2']=1
        hdr['EXPTIME']=0

        DiskHDU=fits.PrimaryHDU(image.astype('float32'),header=hdr)
        DiskHDU.writeto(basename+ ".fit" ,overwrite=True)
    
    def read_image(self,filename):
        log=self.log
        
        if os.path.exists(filename) is False :
            log.text( "**** " + _("the file doesn't exist :"),filename)
            self.ViewInfo()
            return None
        
        try:
            img=cv2.imread(filename,  cv2.IMREAD_ANYDEPTH )
        except: # pylint: disable=bare-except
            log.text( "**** " + _("the file can't open"), ":", filename)
            self.ViewInfo()
            return None

        if img is None :
            log.text( "**** " + _("the image is bad"), ":", filename)
            self.ViewInfo()
            
        return img

    # --------------------------------------------------------------------------
    # recherche la meilleur image de la sequence video
    def SearchingBestImage(self, mode=2):
        log=self.log
        serfd = self.serfd
        splt=self.plot
                
        log.text('')
        log.ligne('-')
        log.titre('-', _("Processing: ") + 'SearchingBestImage()' + ' ... ' ) 

        log.insert( _("searching position") + " . . ."  )

        log.SetStepNumber(serfd.getLength())
        log.SetProgressBar(0)
        frameDimension=serfd.getHeight()*serfd.getWidth()
        sig = np.zeros(shape=serfd.getLength())
        with open( self.videoname, 'rb') as file:  
            file.seek(178) # offset header SER     
            for no in range(0,serfd.getLength()):
                frame=np.fromfile(file, dtype='uint16',count=frameDimension)
                sig[no]= np.sum(frame.astype('float'))
                log.SetProgressBar(no/serfd.getLength()*100)
        log.SetFinishBar()
        log.insert( " . . . "  )
        maxvalue  = np.amax(sig)
        minvalue  = max(1,np.amin(sig))
        selectimg = np.where(sig == maxvalue )[0][0]
        log.text( _("the best position is") + ":" , selectimg )
        
        self.data["best"]=selectimg
        
        filename_best=self.racine_out + "_" + str(selectimg)
        serfd.setCurrentPosition(selectimg)
        image=serfd.read()[0]
        self.save_png_fit(filename_best, image )
        self.data["xzoom"]=self.ZoomOnSun_axis_x( image )
                  
        FullScale=65535.0*image.shape[0]*image.shape[1]
        max_dBfs=20*np.log10(maxvalue/FullScale)
        min_dBfs=20*np.log10(minvalue/FullScale)
        sig[np.where(sig==0)]=1 # evite log10(0)
        #estimation du dark et signal du soleil
        dark_sun=self.dark_signal(sig/maxvalue,1024,[0,1.0],win_average=15, blur_size=0, plot=False)
        if dark_sun is None :
            # si echec sur l'evaluation du noir
            dark_threshold=max_dBfs-(max_dBfs-min_dBfs)/2
            sig_threshold= max_dBfs-6
        else:
            dark_threshold=20*np.log10(dark_sun[0]*maxvalue/FullScale)+6
            sig_threshold= 20*np.log10(dark_sun[1]*maxvalue/FullScale)-6
            
        sig=20*np.log10(sig/FullScale)
        try:
            ytop = int(np.where(sig[0:max(0,selectimg-1)] < dark_threshold )[0][-1])
        except:  # pylint: disable=bare-except
            ytop=0
        try:
            ybottom = int(np.where(sig[selectimg:] < dark_threshold )[0][0])+selectimg
        except:  # pylint: disable=bare-except
            ybottom = serfd.getLength()
            
        self.data["yzoom"]=[ytop,ybottom]
        
        log.text( _("Zoom on Sun")+": axis x =", self.data["xzoom"] )
        log.text( _("Zoom on Sun")+": axis y =", self.data["yzoom"] )
        
        #print("dark_threshold,sig_threshold=", (dark_threshold,sig_threshold))
        selectimg0 = int(np.where(sig[0:max(0,selectimg-1)] < sig_threshold )[0][-1])
        try:
            selectimg1 = int(np.where(sig[selectimg:] <sig_threshold )[0][0])+selectimg
        except:  # pylint: disable=bare-except
            selectimg1 = selectimg+1
        log.text( _("at")+ " %.1f" %( sig_threshold, ) + "dB, "+ _("positions are") + ": %d - %d (%d)"% 
                  (selectimg0,selectimg1,selectimg1-selectimg0+1) )
        log.text( _("the best position is saving as") + ": " 
                  + filename_best + ".png, " + filename_best + "fit"  )
        
        log.SetProgressBar(0)
        nb_img=selectimg1-selectimg0+1
        frameDimension=serfd.getHeight()*serfd.getWidth()
        bytesPerPixels = 1 if (serfd.getHeader()['PixelDepthPerPlane'] <= 8) else 2
        with open( self.videoname, 'rb') as file:  
            file.seek(178+selectimg0*frameDimension*bytesPerPixels) # offset header SER  
            image=np.fromfile(file, dtype='uint16',count=frameDimension).astype('float32')   
            for no in range(selectimg0,selectimg1+1):
                image+=np.fromfile(file, dtype='uint16',count=frameDimension).astype('float32')
                log.SetProgressBar(no/nb_img*100)
            image=np.reshape(image,(serfd.getHeight(),serfd.getWidth()) )
            image/=nb_img
        
        filename_cumul=self.racine_out + "_cumul" 
        self.save_png_fit(filename_cumul, image )
        log.text( _("the cumuled image is saving as") + ": " 
                  + filename_cumul + ".png, " + filename_cumul + "fit"  )
  
        if  mode==0 :
            splt.clear('all')
            splt.add_subplot(3,1,1)
            fr= np.arange(0,serfd.getLength())
            splt.plot(fr,sig,'r')
            splt.plot([ selectimg, selectimg],[0, -5+min_dBfs],'b',
                      label=_("best position:")+str(selectimg))
            middle=int(serfd.getLength()/2)
            splt.plot([ middle, middle],[0, -5+min_dBfs],'g',
                      label=_("middle position:")+str(middle))
            splt.ylabel(_("signal level (dB FS)"))
            splt.title(_("signal level / frame")+ " (max=%3.1f dB FS)" %(max_dBfs,))
            splt.legend()
            splt.grid(True)
            
            serfd.setCurrentPosition(selectimg)
            image,numero=serfd.read()
            splt.add_subplot(3,1,2)
            splt.imshow(image,cmap='gray' )
            splt.title(_("Best frame no:")+ str(numero))

            splt.add_subplot(3,1,3)
            splt.imshow(image,cmap='gray' )
            splt.title(_("Frame no:")+ str(selectimg0)+'-'+ str(selectimg1))   
            splt.xlabel(_("frame number"))
            splt.maximize(True)
          
            self.ViewImage()
            splt.draw()
            splt.pause()
           
        self.SaveData()     
        return selectimg
    # --------------------------------------------------------------------------
    # Zoom le soleil selon x-axis
    def ZoomOnSun_axis_x(self, image, plot=False):
        #log=self.log
        hh,ww=image.shape

        line_sum=np.zeros(ww).astype(float)
        for yy in range(hh):
            line_sum=line_sum + image[yy,:]
        
        vmin=np.min(line_sum)
        vmax=np.max(line_sum)
            
        dark_sun=self.dark_signal(line_sum/vmax,1024,[0,1.0],win_average=15, blur_size=0, plot=False)
        if dark_sun is None :
            dark_threshold=vmin+(vmax-vmin)/6
        else:
            dark_threshold=dark_sun[0]*vmax*2 # 6dB au dessus du fond
                        
        ratio_win= 1.0/2.0
        win_end=int(line_sum.size*ratio_win-1)
        left_edge = np.where(line_sum[0:win_end] <dark_threshold )

        win_begin = int(line_sum.size*(1.0-ratio_win))
        right_edge = np.where(line_sum[win_begin:line_sum.size-1] <dark_threshold )
        
        if len(left_edge[0]) == 0 :
            left_edge=0
        else:
            left_edge=left_edge[0][-1]
            
        if len(right_edge[0]) == 0 :
            right_edge=ww-1
        else:
            right_edge=right_edge[0][0]+ win_begin

        zoom_win = [left_edge,right_edge]

        if  plot  : 
            splt=self.plot
            splt.clear('all')
            splt.add_subplot(2,1,1)
            splt.plot(line_sum,'r')
            splt.plot([0,ww-1],[dark_threshold,dark_threshold],'g')
            splt.plot([left_edge,left_edge],[0,vmax*1.1],'b',label=_("left")+":"+str(left_edge))
            splt.plot([right_edge,right_edge],[0,vmax*1.1],'b',label=_("right")+":"+str(right_edge))
            splt.legend()
            splt.add_subplot(2,1,2)
            splt.imshow(image,cmap='gray' )
            splt.maximize(True)
            splt.draw()
            self.ViewImage()
            splt.pause()
             
        return zoom_win

    # --------------------------------------------------------------------------
    # Estimation de la distorsion du spectre
    def SpectrumDistorting(self, no_img, mode=2): 
        log=self.log
        serfd = self.serfd
        splt=self.plot
                
        log.text('')
        log.ligne('-')
        log.titre('-', _("Processing: ") + 'SpectrumDistorting()' + ' ... ' ) 
        
        if  self.CheckNumberFrame(no_img): return None

        serfd.setCurrentPosition(int(no_img))
        image=serfd.read()[0]

        # zoom sur la partie utile
        if len(self.data["xzoom"]) == 0 :
            self.log.text( "**** " + _("xzoom is not defined: launch")+ " '"+_("Search best position")+"'")
            self.ViewInfo()
            return None
        
        (left_edge,right_edge) = self.data["xzoom"]

        nb_points=3
        zw=int((right_edge-left_edge)/nb_points)   

        splt.clear('all')
        msg=[_("first"),_("second"),_("third")]
        for no in  range(0,nb_points):
            image_zoom = image[:,left_edge+no*zw:left_edge+(no+1)*zw]
            splt.add_subplot(nb_points,1,no+1)
            splt.imshow(image_zoom, cmap='gray')
            splt.title(_("Select a %s point with the middle mouse button")%(msg[no],) )
        splt.maximize()
        self.ViewImage()
        splt.draw()
        xy_points = splt.ginput(n=nb_points,timeout=300,mouse_add=plt.MouseButton.MIDDLE, mouse_pop=plt.MouseButton.RIGHT, mouse_stop=None)
        if len(xy_points) < nb_points :
            log.text( "**** " + _("you have to select %d points")%(nb_points,), ":", xy_points)
            self.ViewInfo()
            return None

        log.text(_("Selection points") + ":",xy_points )
        x_pts = np.zeros(shape=nb_points)
        y_pts = np.zeros(shape=nb_points)
        for no in  range(0,nb_points):
            x_pts[no]=xy_points[no][0]+left_edge+no*zw
            y_pts[no]=xy_points[no][1]
            
        y_mod = self.modelisation_parabole(x_pts,y_pts, image,mode==0)
        self.data["y_mod"]=y_mod
        self.SaveData()     
        return y_mod 
 
    # ----------------------------------------------------------------------
    # Utilisation de la fonction polyfit pour determiner les parametres de
    # la modelisation parabolique de la fente : y = a.x2 + b.x + c
    def modelisation_parabole(self,x_pts,y_pts,image, plot=False ):
        log=self.log
        splt=self.plot
        
        coef_interpo=np.polyfit(x_pts,y_pts,2).tolist()
        self.data["parabola"]=coef_interpo

        log.text(_("Parabola coefficient") + ":" ,coef_interpo)

        ww=image.shape[1]
        xmod = np.arange(0,ww)
        ymod = [coef_interpo[2] + coef_interpo[1] * val + coef_interpo[0] * val**2 for val in xmod]  # Calcul du modele

        if plot : 
            splt.clear('all')
            splt.add_subplot(3,1,2)
            text_label=f"Modelisation : y = {coef_interpo[0]:.3f}x2 + {coef_interpo[1]:.3f}x + {coef_interpo[2]:.3f}"
            splt.scatter(x_pts, y_pts, marker = "o", color = "blue", label = "Positions")
            splt.plot(xmod, ymod, color = "red", label = text_label ) 
            splt.xlabel(_("x-axis"))
            splt.ylabel(_("y-axis"))
            splt.invert_yaxis()
            splt.title(_("modeling by a parabola"))
            splt.add_subplot(3,1,1)
            splt.imshow(image, cmap='gray')
            splt.add_subplot(3,1,3)
            splt.plot(xmod, ymod, color = "red", label = text_label) 
            splt.imshow(image, cmap='gray')
            splt.plot(xmod, ymod, color = "red", label = text_label)  
            splt.draw()
            self.ViewImage()
            splt.pause()            

        return ( ymod )
    # --------------------------------------------------------------------------
    # Estimation automatique de la distorsion du spectre
    def SpectrumDistortingAuto(self, mode=2): 
        log=self.log
        splt=self.plot
                
        log.text('')
        log.ligne('-')
        log.titre('-', _("Processing: ") + 'SpectrumDistortingAuto()' + ' ... ' ) 
        
        filename_cumul=self.racine_out + "_cumul.png"
        img = self.read_image(filename_cumul)
        if img is None :  return None
            
        if len(self.data["xzoom"]) == 0 :
            self.log.text( "**** " + _("xzoom is not defined: launch")+ " '"+_("Search best position")+"'")
            self.ViewInfo()
            return None
            
        # zoom sur la partie utile
        (left_edge,right_edge) = self.data["xzoom"]
        
        nww=right_edge-left_edge+1
        
        if nww > 150:
            right_edge-=25
            left_edge+=25
        
        x_pts=[]
        y_pts=[]
        for xx in range(left_edge,right_edge):
            yy_min = img[:,xx].argmin()
            x_pts.append(xx )
            y_pts.append(yy_min)
                        
        yt=np.array(y_pts)
        std_value=np.std(yt)
        med_value=np.median(yt)
        
        x2_pts=[]
        y2_pts=[]
        for ii in range(0,len(x_pts)):
            if abs(y_pts[ii]-med_value) < std_value/2 :
                x2_pts.append(x_pts[ii] )
                y2_pts.append(y_pts[ii])
                            
        if False: # pylint: disable=using-constant-test
            splt.clear('all')
            splt.add_subplot()
            splt.imshow(img, cmap='gray')
            splt.scatter(x2_pts, y2_pts, marker = "o", color = "blue", label = "Positions")
            splt.maximize(True)
            self.ViewImage()
            splt.draw()
            splt.pause()
                        
        y_mod = self.modelisation_parabole(x2_pts,y2_pts, img,mode==0)
        self.data["y_mod"]=y_mod
        self.SaveData()     
        return y_mod 

    # ---------------------------------------------------------------------------
    # correction de la distorsion pour obtenir une 1iere image brute
    def SlitCorrectionProcess(self, no_img, ymod, mode=2): # pylint: disable=unused-argument
        log=self.log
        serfd = self.serfd
        splt=self.plot
                
        log.text('')
        log.ligne('-')
        log.titre('-', _("Processing: ") + 'SlitCorrectionProcess()' + ' ... ' ) 
        
        if self.CheckNumberFrame(no_img): return None
        if ymod == None or len(ymod)==0:
            log.text( "**** " + _("modelisation is not defined"))
            self.ViewInfo()
            return None

        serfd.setCurrentPosition(int(no_img))
        image=serfd.read()[0]

        d_min_max=int(np.amax(ymod)-np.amin(ymod))
        image = np.pad(image, [[d_min_max,d_min_max], [0,0]], 'constant')
        hh,ww=image.shape

        # calcul de la correction
        dymod=ymod-np.amin(ymod)
        lut0=np.zeros(shape=image.shape)
        lut1=np.zeros(shape=image.shape)
        coef0=np.zeros(shape=image.shape)
        coef1=np.zeros(shape=image.shape)
        for xx in range(0,ww):
            for yy in range(0,hh):
                lut0[yy,xx]=0
                lut1[yy,xx]=0
                yyy_exact=yy+dymod[xx]
                yyy0=int(yyy_exact) # troncature
                yyy1=yyy0+1
                yyy_fract=yyy_exact-yyy0
                if yyy0>=0 and  yyy0 < hh :
                    lut0[yy,xx]= yyy0*ww+xx
                    coef0[yy,xx]=1.0-yyy_fract
                if yyy1>=0 and  yyy1 < hh :
                    lut1[yy,xx]= yyy1*ww+xx
                    coef1[yy,xx]=yyy_fract
        lut0=lut0.astype(int)
        lut1=lut1.astype(int)

        # ----------------------------------------------------------------------
        # applique la correction
        image_corr0=image.flat[lut0] * coef0
        image_corr1=image.flat[lut1] * coef1
        image_corr=image_corr1+image_corr0
        img_zoom=image_corr[:,int(ww/3):int(2*ww/3)]

        if self.data["parabola"][0]>0 :
            sommet=round(max(ymod))
        else:
            sommet=round(min(ymod))+d_min_max
        if  mode == 2 :
            xy_point=[(img_zoom.shape[1],sommet)]
        else:
            splt.clear('all')
            splt.add_subplot()
            splt.imshow(img_zoom, cmap='gray')
            splt.plot([0,img_zoom.shape[1]-1],[sommet,sommet],'b')
            splt.title(_("Selection of spectral line with the middle mouse button")+"\n"+_("or Escape key for default position") )
            splt.maximize(True)
            self.ViewImage()
            splt.draw()
            xy_point = splt.ginput(n=1,timeout=300,mouse_add=plt.MouseButton.MIDDLE, mouse_pop=plt.MouseButton.RIGHT, mouse_stop=None)
            if len(xy_point) < 1 :
                xy_point=[(img_zoom.shape[1],sommet)]
                log.text( "**** " + _("no point selection"), ":", _("default point="), xy_point)
                self.ViewInfo()
            else:
                log.text(_("Selection points") + ":",xy_point )
        offset=int(xy_point[0][1])

        filename_correct=self.racine_out + "_" + str(no_img) + "_corrected"
        self.save_png_fit(filename_correct, image_corr)
        log.text( _("the corrected image is saving as") + ": " 
                  + filename_correct + ".png, " + filename_correct + ".fit")

        # image raw scan
        serfd.setCurrentPosition(0)
        scanraw=np.zeros(shape=(serfd.getLength(),ww))
        ratio0=coef0[offset,:]
        ratio1=coef1[offset,:]
        log.SetStepNumber(serfd.getLength())
        log.SetProgressBar(0)
        vi_max=0
        for no in range(0,serfd.getLength()):
            log.SetProgressBar(no/serfd.getLength()*100)
            image=serfd.read()[0]
            image = np.pad(image, [[d_min_max,d_min_max], [0,0]], 'constant')
            vi_max=max(vi_max,np.amax(image))
            line_corr0=image.flat[lut0[offset,:]] * ratio0
            line_corr1=image.flat[lut1[offset,:]] * ratio1
            scanraw[no,:]=line_corr0+line_corr1
        log.SetFinishBar()
        
        scanraw=rescale(scanraw,65535)

        log.text( "SCAN OFFSET:", offset )

        if  mode == 0 :
            splt.clear('all')
            splt.add_subplot()
            splt.imshow(scanraw, cmap='gray')
            splt.title(_("RAW image: ") + _("offset") + "=" + str(offset) )
            splt.draw()
            splt.maximize(True)
            self.ViewImage()
            splt.pause()

        filename_raw=self.racine_out + "_offset_" + str(offset)
        self.save_png_fit(filename_raw, scanraw)
        log.text( _("the raw image is saving as") + ": " 
                  + filename_raw + ".png, " + filename_raw + ".fit")

        return filename_raw+".png"
        
    # --------------------------------------------------------------------------
    # correction de transversallium
    def TransversalliumCorrection(self, raw_filename, mode=2 ) :
        log=self.log
        splt=self.plot
                
        log.text('')
        log.ligne('-')
        log.titre('-', _("Processing: ") + 'TransversalliumCorrection()' + ' ... ' ) 
        
        if len(self.data["xzoom"]) == 0 or len(self.data["yzoom"]) == 0 :
            log.text( "**** " + _("x/y zoom is not defined: launch")+ " '"+_("Search best position")+"'")
            self.ViewInfo()
            return None

        (left_edge,right_edge) = self.data["xzoom"]
        (top_edge,bottom_edge) = self.data["yzoom"]

        img = self.read_image(raw_filename)
        if img is None :  return None
            
        (hh,ww)= img.shape

        lines=np.zeros(shape=ww)
        mediane=np.median(img[top_edge:bottom_edge,left_edge:right_edge])
        std_value = np.std(img[top_edge:bottom_edge,left_edge:right_edge])
        seuil = 1.5*std_value 

        dark_sun = self.dark_signal(img[top_edge:bottom_edge,left_edge:right_edge],1024,[0,65536],win_average=15, blur_size=53, plot=False)
        if dark_sun is not None:
            log.text("dark=",dark_sun[0], " sun=",dark_sun[1])
            if mediane-seuil < 2*dark_sun[0] :
                new_seuil= mediane-2*dark_sun[0]
                log.text("Forcing Threshold: ", seuil, "=>", new_seuil )
                seuil =new_seuil
        log.text("median=",mediane, " std=",std_value, " seuil=",seuil)
        log.text( "[borne_inf:borne_sup] =",[mediane-seuil,mediane+seuil])
        for xx in range(ww):  
            lines[xx]=0
            for yy in range(hh):
                if abs(img[yy,xx]-mediane)< seuil:
                    lines[xx]+= img[yy,xx]
                else:
                    lines[xx]+=mediane               
            lines[xx]=lines[xx]/hh/mediane
             
        order=4
        mode_bord='mirror'               
        gain=1.5
        lines=(1-lines)*gain
        lines[0:left_edge+1] =0
        lines[right_edge:] =0
        lines_smoothed=np.copy(lines)
        for ii in range(4):
            lines_smoothed=signal.savgol_filter(lines,(1+ii)*100+1, order,mode=mode_bord)
            lines_smoothed[0:left_edge+1] =0
            lines_smoothed[right_edge:] =0

        lines_smoothed=1-lines_smoothed
        lines=1-lines               
        hf=np.divide(lines,lines_smoothed)
        
        img_c=np.copy(img).astype('float32')
        for yy in range(0,hh):        
            img_c[yy,:] = np.divide(img[yy,:],hf)
            
        img_c=rescale(img_c,65535)
            
        if  mode==0 :
            splt.clear('all')
            splt.add_subplot(3,1,1)
            if img.shape[0]<img.shape[1]:
                splt.imshow(img, cmap='gray')
            else:
                splt.imshow(np.rot90(img), cmap='gray')
            splt.add_subplot(3,1,2)
            splt.plot(lines,label='lines')
            splt.plot(lines_smoothed,label='lines_smoothed')
            splt.plot(hf,label='hf')
            splt.grid()
            splt.legend()
            splt.add_subplot(3,1,3)
            if img.shape[0]<img.shape[1]:
                splt.imshow(img_c, cmap='gray')
            else:
                splt.imshow(np.rot90(img_c), cmap='gray')
            splt.title(_('Transversallium Correction'))
            splt.maximize(True)
            splt.draw()
            self.ViewImage()
        
        if  False : # pylint: disable=using-constant-test
            splt.clear('all')
            splt.add_subplot()
            splt.imshow(img_c, cmap='gray')
            splt.title(_('Transversallium Correction'))
            splt.maximize(True)
            splt.draw()
            self.ViewImage()
            splt.pause()
                    
        filename_tr=os.path.splitext(raw_filename)[0] + "-t"
        self.save_png_fit(filename_tr, img_c)
        log.text( _("the raw image is saving as") + ": "
                  + filename_tr + ".png, " + filename_tr + ".fit")
       
        return filename_tr+".png"

    # --------------------------------------------------------------------------
    # Reconstruit un soleil circulaire
    def ReshapeRawSun(self, raw_filename, mode=2 ) :
        log=self.log
        splt=self.plot
        
        log.text('')
        log.ligne('-')
        log.titre('-', _("Processing: ") + 'ReshapeRawSun()' + ' ... ' ) 
        
        img = self.read_image(raw_filename).astype('float32')
        if img is None :  return None
       
        if mode == 0 :
            ret=self.ManualEdgeSelection(img)
        else:
            ret=self.AutoEdgeSelection(img)
            
        if ret is None : return None        
        x,y= ret
        ratio, angle_degre, center = self.EstimEllipse(img, x, y ,mode)[0:3]

        imgsun=Eclipse_to_circle(img,ratio, angle_degre, center)
        imgsun=rescale(imgsun,65535)
        self.log.text(_("circularizes the image") + ":" )
        self.log.text( " * " + _("Old image")+": ih x iw =", img.shape[0]   ,"x", img.shape[1] )
        self.log.text( " * " + _("New image")+": ih x iw =", imgsun.shape[0],"x", imgsun.shape[1] )           
        
        if mode >=1 :
            ret=self.AutoEdgeSelection(imgsun)            
            if ret is not None :        
                x,y= ret
                ratio, angle_degre, center = self.EstimEllipse(imgsun, x, y ,mode)[0:3]
                err = 1/ratio-1 if ratio < 1  else ratio-1
                log.text( _("corrected error: "), " %.2f%%" % (err*100 , ) )
                if err < 0.01 :
                    log.text( _("no correction") )
                elif err >= 0.01 and err < 0.2 :
                    imgsun=Eclipse_to_circle(imgsun,ratio, angle_degre, center)
                    imgsun=rescale(imgsun,65535)
                    log.text( _("second correction") ) 
                else: # err > 0.2 : # Echec passage en mode manuel ,  on repart de l'image de base
                    log.text( _("auto disk selection failed") + ": " + _("error") 
                              + "=%.2f%%"%(err*100,) )
                    log.text( _("switch to manual mode") ) 
                    x,y=self.ManualEdgeSelection(img)
                    ratio, angle_degre, center = self.EstimEllipse(img, x, y ,mode)[0:3]
                    imgsun=Eclipse_to_circle(img,ratio, angle_degre, center)
                    imgsun=rescale(imgsun,65535)

        if mode<2 :
            splt.clear('all')
            splt.add_subplot()
            splt.imshow(imgsun, cmap='gray')
            splt.title(_("Sun circularized"))
            splt.maximize(True)
            splt.draw()
            self.ViewImage()
            splt.pause()

        filename_sun=os.path.splitext(raw_filename)[0] + "-sun"
        self.save_png_fit(filename_sun, imgsun)
        log.text( _("the sun image is saving as") + ": " 
                  + filename_sun + ".png, " + filename_sun + ".fit" )
                
        return filename_sun + ".png"

    # --------------------------------------------------------------------------
    def EstimEllipse(self, img, x, y ,mode=1):
        log=self.log
        splt=self.plot
        if mode<2 :
            splt.clear('all')
            splt.add_subplot()
            splt.imshow(img, cmap='gray')
            splt.scatter(y,x, marker = "+", color = "blue", label = "Positions")
            splt.title(_("edge detection"))
            splt.maximize(True)
            splt.draw()
            self.ViewImage()
            splt.pause()
        xy_points=list(zip(x,y))
        XY=np.array(xy_points)
        reg = LsqEllipse().fit(XY.astype(float))
        center_yx, height, width, phi = reg.as_parameters()            
        ratio= width /height
        angle_degre=np.rad2deg(phi)
        log.text( _("Ellipse parameters : "))
        log.text( "   * " + _("center, width, height : "), (center_yx, width, height))
        log.text( "   * " + _("ratio SW/SH : "), ratio )
        log.text( "   * " + _("phi: rad , deg: "), ( phi, angle_degre ))
        
        if angle_degre > 45  : 
            angle_degre=angle_degre-90
            (width,height)=(height,width)
        if angle_degre < -45 : 
            angle_degre=angle_degre+90
            (width,height)=(height,width)
        ratio= width /height
        log.text("   => " + _("corrected phi en deg: "), angle_degre )
        log.text("   => " + _("corrected ratio: "), ratio )
        
        if False : # pylint: disable=using-constant-test
            splt.clear('all')
            splt.add_subplot()
            splt.imshow(img, cmap='gray')
            splt.scatter(y,x, marker = "o", color = "blue", label = "Positions")
            ellipse = Ellipse(
                xy=(center_yx[1],center_yx[0]), width=2*width, height=2*height, angle=angle_degre,
                edgecolor='g', fc='None', lw=2, label='Fit', zorder=2
                )
            splt.plot(center_yx[1],center_yx[0],"xg")
            splt.add_patch(ellipse)
            splt.title(_("edge detection"))
            splt.maximize(True)
            splt.draw()
            self.ViewImage()
            splt.pause()
            
        return ratio, angle_degre, center_yx, height, width 

    # --------------------------------------------------------------------------
    def ManualEdgeSelection(self, img):
        log=self.log
        splt=self.plot
        nb_points=9
        splt.clear('all')
        splt.add_subplot()
        splt.imshow(img, cmap='gray')
        splt.title(_("Select %s point with the middle mouse button")%(nb_points,) )
        splt.maximize()
        self.ViewImage()
        splt.draw()
        xy_points = splt.ginput(n=nb_points,timeout=300,mouse_add=plt.MouseButton.MIDDLE, mouse_pop=plt.MouseButton.RIGHT, mouse_stop=None)
        if len(xy_points) < nb_points :
            log.text( "**** " + _("you have to select %d points")%(nb_points,), ":", xy_points)
            self.ViewInfo()
            return None

        log.text(_("Selection points") + ":",xy_points )
        x_pts = np.zeros(shape=nb_points)
        y_pts = np.zeros(shape=nb_points)
        for no in  range(0,nb_points):
            x_pts[no]=xy_points[no][0]
            y_pts[no]=xy_points[no][1]
            
        return y_pts, x_pts
    
    # --------------------------------------------------------------------------
    def AutoEdgeSelection(self,img):
        log=self.log
        splt=self.plot
        
        dark_sun_all = self.dark_signal(img,1024,[0,65536],win_average=15, blur_size=31, plot=False)
        
        (left_edge,right_edge) = self.ZoomOnSun_axis_x(img)
        (top_edge,bottom_edge) = (0,img.shape[0]-1)
        marge=50 # marge pour ne pas prendre en compte  le debut ou la fin de disque
        if right_edge-left_edge < 3*marge :
            marge=0
            
        img_cpy=np.copy(img)
        for yy in range(img.shape[0]):
            line_yy=np.copy(img[yy,:])
            win_average=6
            line_yy_sma=movingaverage(line_yy,win_average,'same')
            histo,bins = np.histogram(line_yy_sma,256,[0,65536])
            idx_peaks,_n=signal.find_peaks(histo, prominence=1, height=5)
            if len(idx_peaks) <= 1 :
                dark_level=dark_sun_all[0]
            else:
                dark_level=(bins[idx_peaks[0]]+bins[idx_peaks[0]+1])/2
                
            if False :  # pylint: disable=using-constant-test 
            # if  yy==int(img.shape[0]/2) :
                print(yy, "* dark_level:", dark_level, " :", idx_peaks)
                splt.clear('all')
                splt.add_subplot()
                splt.plot(histo,'r')                
                splt.plot(idx_peaks,histo[idx_peaks],'xb')
                #peak_widths(x, peaks, rel_height=0.5)
                splt.maximize(True)
                splt.draw()
                self.ViewImage()
                splt.pause()
            pts_found= np.where( line_yy_sma >  dark_level*4 )   
            if pts_found is not None :  line_yy[pts_found]=65535
            pts_found=np.where( line_yy_sma <= dark_level*2 )
            if pts_found is not None : line_yy[pts_found]=0
            pts_found=np.where( (line_yy_sma < dark_level*2) & (line_yy_sma >= dark_level*4) ) 
            if pts_found is not None : line_yy[pts_found]=dark_level*2
            img_cpy[yy,:]=line_yy
        # print("img dark_level:", dark_sun_all[0], " :", dark_sun_all[0]/256, img.shape)
        
        img_zoom=np.copy(img_cpy[top_edge:bottom_edge,left_edge+marge:right_edge-marge])
        imgedge= ndimage.prewitt(img_zoom,mode='mirror')
        XY1=np.where(abs(imgedge) > 32768 )
        #x=XY1[0].tolist()
        y=XY1[1].tolist()
        x=[]
        for elt in XY1[0].tolist() :
            x.append(elt+top_edge)
        
        if len(x) == 0 :
            log.text( _("edge detection is aborted")  )
            return None
               
        if False : # pylint: disable=using-constant-test
            splt.clear('all')
            splt.add_subplot()
            splt.imshow(imgedge, cmap='gray')
            splt.scatter(y,x, marker = "+", color = "blue", label = "Positions")
            splt.title(_("edge detection"))
            splt.maximize(True)
            splt.draw()
            self.ViewImage()
            splt.pause()
            #return None
            
        for ii in range(0,len(y)):
            y[ii]+=left_edge+marge

        return x,y
     
    # --------------------------------------------------------------------------
    def HighContrastProcessing(self, sun_filename, coef_low=0.15, percent_high=99.999, mode=2 ):
        log=self.log
        splt=self.plot
        
        log.text('')
        log.ligne('-')
        log.titre('-', _("Processing: ") + 'HighContrastProcessing()' + ' ... ' ) 
        
        img = self.read_image(sun_filename)
        if img is None :  return None
        
        Seuil_haut=np.percentile(img,percent_high)
        Seuil_bas=(Seuil_haut*coef_low)
        img[img>Seuil_haut]=Seuil_haut
        img[img<=Seuil_bas]=Seuil_bas
        img=rescale((img-Seuil_bas),65536)
        
        filename_hc=os.path.splitext(sun_filename)[0] + "-hc"
        self.save_png_fit(filename_hc, img)
        log.text( _("the high contrast image is saving as") + ": " 
                  + filename_hc + ".png, " + filename_hc + ".fit" )
                       
        if  mode==0 :
            splt.clear('all')
            splt.add_subplot()
            splt.imshow(img, cmap='gray')
            splt.title(_("Sun - High-contrast"))
            splt.maximize(True)
            splt.draw()
            self.ViewImage()
            #splt.pause()
            
        return filename_hc + ".png"
    
    # --------------------------------------------------------------------------
    def ProtuberanceProcessing(self, sun_filename, coef_high=0.5, percent_high=99.999, mode=2 ):
        log=self.log
        splt=self.plot
        
        log.text('')
        log.ligne('-')
        log.titre('-', _("Processing: ") + 'ProtuberanceProcessing()' + ' ... ' ) 
        
        img = self.read_image(sun_filename).astype('float32')
        if img is None :  return None
        
        marge=10 # detourage pour eviter des effet sur les bords d'image   
        (top_edge, bottom_edge)=(0+marge,img.shape[0]-1-marge)
        (left_edge,right_edge) =(0+marge,img.shape[1]-1-marge)
        img_zoom=img[top_edge:bottom_edge,left_edge:right_edge]
        
        ret = self.AutoEdgeSelection(img_zoom)            
        if ret is None : return None        
        x,y= ret
        center, height, width = self.EstimEllipse(img_zoom, x, y ,mode)[2:5]
        
        err= (height/width)-1 if height > width else (width/height)-1
        if err > 0.01: 
            log.text( _("auto disk selection failed") + ": " + _("error") 
                      + "=%.2f%%"%(err*100,) )
            log.text( _("switch to manual mode") ) 
            ret = self.ManualEdgeSelection(img_zoom)            
            if ret is None : return None        
            x,y= ret
            center, height, width = self.EstimEllipse(img_zoom, x, y ,2)[2:5]
                    
        xcircle=round(center[1]+left_edge)
        ycircle=round(center[0]+top_edge)
        rcircle=round((height+width)/2)
        
        img=cv2.circle(img, (xcircle,ycircle), rcircle,0,-1,lineType=cv2.LINE_AA)        
            
        Seuil_haut=np.percentile(img,percent_high)*coef_high
        Seuil_bas=0
        img[img>Seuil_haut]=Seuil_haut
        img[img<=Seuil_bas]=Seuil_bas
        img=rescale((img-Seuil_bas),65536)
        
        #img=cv2.rectangle(img, (left_edge,top_edge), (right_edge,bottom_edge),255,-1)        
        
        filename_protu=os.path.splitext(sun_filename)[0] + "-protu"
        self.save_png_fit(filename_protu, img)
        log.text( _("the protuberance image is saving as") + ": " 
                  + filename_protu + ".png, " + filename_protu + ".fit" )
                       
        if  mode==0 :
            splt.clear('all')
            splt.add_subplot()
            splt.imshow(img, cmap='gray')
            splt.title(_("Sun - Protuberance"))
            splt.maximize(True)
            splt.draw()
            self.ViewImage()
            #splt.pause()
            
        return filename_protu + ".png"
        
    # --------------------------------------------------------------------------
    def ClaheProcessing(self, sun_filename, clip=2, tile_sz=8, mode=2 ):
        log=self.log
        splt=self.plot
        
        log.text('')
        log.ligne('-')
        log.titre('-', _("Processing: ") + 'ClaheProcessing()' + ' ... ' ) 
        
        img = self.read_image(sun_filename)
        if img is None :  return None
                
        if  mode==0 :
            splt.clear('all')
            splt.add_subplot()
            splt.imshow(img, cmap='gray')
            splt.title(_("Sun "))
            splt.maximize(True)
            splt.draw()
            self.ViewImage()
            splt.pause()          
        
        clahe = cv2.createCLAHE(clipLimit =clip, tileGridSize=(tile_sz,tile_sz))
        cl_img = clahe.apply(img)
        
        if  mode==0 :
            splt.clear('all')
            splt.add_subplot()
            splt.imshow(cl_img, cmap='gray')
            splt.title(_("Sun - CLAHE"))
            splt.maximize(True)
            splt.draw()
            self.ViewImage()
            #splt.pause()
             
        filename_clahe = os.path.splitext(sun_filename)[0] + "-clahe-%3.2f_%dx%d" %(clip,tile_sz,tile_sz)      
        self.save_png_fit(filename_clahe, cl_img)
        log.text( _("the CLAHE sun image is saving as") + ": " 
                  + filename_clahe + ".png, " + filename_clahe + ".fit" )               
        return filename_clahe + ".png"
    
    # --------------------------------------------------------------------------
    # test
    def SignalNoiseImage(self, raw_filename, mode=2 ) :
        log=self.log
                
        log.text('')
        log.ligne('-')
        log.titre('-', _("Processing: ") + 'SignalNoiseImage()' + ' ... ' ) 
        
        img = self.read_image(raw_filename)
        if img is None :  return None
        
        return self.dark_signal(img,1024,[0,65536],win_average=15, blur_size=53, plot=(mode==0))  
    
    def dark_signal(self,img,nb_bins,dynamique,win_average=15, blur_size=41, plot=False, trace=True):
        log=self.log
        if blur_size > 0 :
            img2 = cv2.blur(img,(blur_size,blur_size))
            histo,bins = np.histogram(img2,nb_bins,dynamique)
        else:
            histo,bins = np.histogram(img,nb_bins,dynamique)

        histo_smoothed=movingaverage(histo,win_average)
        # note: la taille de  histo_smoothed = nb_bins+win_average-1 (full convolve)
        idx_peaks,_n=signal.find_peaks(histo_smoothed, prominence=1)
        if len(idx_peaks) <= 1 :
            if trace : log.text( "***", _("No peaks found on histogram"))
            return None
        
        # recherche du Peaks sans  prendre en compte le premier a droite
        peaks_value=np.append(histo_smoothed[idx_peaks],0) # ajoute un 0 pour detecter le  pic de fin
        idx_peaks2,_n=signal.find_peaks(peaks_value, prominence=1)
        if len(idx_peaks2)==0 :
            idx_peak_signal=idx_peaks[1+np.argmax(histo_smoothed[idx_peaks[1:]])]
            w2=0
        else:
            idx_peaks_found=idx_peaks2[np.argmax(histo_smoothed[idx_peaks[idx_peaks2]])] # conserve le max
            w2=int(win_average/2)
            idx_peak_signal=idx_peaks[idx_peaks_found]-w2
        idx_peak_dark=idx_peaks[0]-w2
        dark_level=(bins[idx_peak_dark]+bins[idx_peak_dark+1])/2
        sig_level=(bins[idx_peak_signal]+bins[idx_peak_signal+1])/2
        if trace : log.text( "Dark=",dark_level, " sig level:",sig_level , " S/B=", 20*np.log10(sig_level/dark_level))
        if  plot :
            splt=self.plot
            splt.clear('all')
            splt.add_subplot(2,1,1)
            splt.plot(histo_smoothed[w2:-w2],'b')
            splt.plot(idx_peaks-w2,histo_smoothed[idx_peaks],'xg')
            splt.add_subplot(2,1,2)
            splt.plot(idx_peaks-w2,histo_smoothed[idx_peaks],'xg')
            splt.plot(idx_peak_signal,histo_smoothed[idx_peak_signal+w2],'*r')
            splt.plot(idx_peak_dark,histo_smoothed[idx_peak_dark+w2],'*b')
            splt.title(_("Peaks"))
            splt.maximize(True)
            splt.draw()
            self.ViewImage()
            splt.pause()
        return (dark_level,sig_level)
        
         
    # --------------------------------------------------------------------------
    def plot_final(self,filename_sun="",filename_clahe="",filename_hc="",filename_protu=""):
        splt=self.plot
        nb_plot = 0
        if os.path.isfile(filename_sun)   : nb_plot+=1 
        if os.path.isfile(filename_clahe) : nb_plot+=1 
        if os.path.isfile(filename_hc)    : nb_plot+=1 
        if os.path.isfile(filename_protu) : nb_plot+=1 
        if nb_plot == 0 : return
        racine=sqrt(nb_plot)
        ncols=ceil(racine)
        nrows= ceil(nb_plot/ncols)
           
        splt.clear('all')
        index=1
        index+=self.subplot(filename_sun  , nrows, ncols, index,_("Sun - RAW"))
        index+=self.subplot(filename_clahe, nrows, ncols, index,_("Sun - CLAHE"))
        index+=self.subplot(filename_hc   , nrows, ncols, index,_("Sun - High-contrast"))
        index+=self.subplot(filename_protu, nrows, ncols, index,_("Sun - Protuberance"))
        splt.maximize(True)
        splt.draw()
        self.ViewImage()

            
    def subplot(self,filename, nrows, ncols, index,titre):
        if not os.path.isfile(filename)   : return 0
        img = self.read_image(filename)
        if img is None : return 0
        if img.shape[0] > img.shape[1] : img=np.rot90(img)
        splt=self.plot
        splt.add_subplot(nrows, ncols,index)
        splt.imshow(img, cmap='gray')
        splt.axis('off')
        splt.title(titre) 
        return 1   
    
    # --------------------------------------------------------------------------
    def limit_image_1dim(self, img, sens, valuemax, seuil ) :
        hh,ww= img.shape
        if sens :
            sz=hh
            max_lines=np.zeros(shape=sz)
            for ii in range(0,sz):  max_lines[ii]=max(1,np.amax(img[ii,:]))
        else:
            sz=ww
            max_lines=np.zeros(shape=sz)
            for ii in range(0,sz):  max_lines[ii]=max(1,np.amax(img[:,ii]))
        max_lines=20*np.log10(max_lines/valuemax)

        win_size=2*max_lines.size/3
        limit_end=int(win_size)
        limit_begin=int(max_lines.size-win_size)
        first_edge=np.where(max_lines[:limit_end]  < seuil )
        last_edge =np.where(max_lines[limit_begin:] < seuil )
        if last_edge[0].size == 0 or first_edge[0].size ==0 :
            first_edge=0
            last_edge=sz
        else:
            first_edge=first_edge[0][-1]
            last_edge=last_edge[0][0]+ limit_begin
        mean_value=np.mean(max_lines[first_edge:last_edge])
        med_value=np.median(max_lines[first_edge:last_edge])
        std_value=np.std(max_lines[first_edge:last_edge])
        return (first_edge,last_edge,(mean_value,med_value,std_value) )

