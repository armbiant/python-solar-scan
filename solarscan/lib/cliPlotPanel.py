#! /usr/bin/env python

# -*- coding: UTF-8 -*-

# ------------------------------------------------------------------------------
# Project: solarscan ( Solar Scan processing )
#
# This script processes the video solar acquisition of the sun made with the
# instrument Sol'Ex (http://www.astrosurf.com/solex/).
#
# ------------------------------------------------------------------------------
#    Author:  M27trognondepomme <pebe92 (at) gmail.com>
#
# This program is provided without any guarantee.
#
# The license is  LGPL-v3
# For details, see GNU General Public License, version 3 or later.
#                        "https://www.gnu.org/licenses/gpl.html"
# ------------------------------------------------------------------------------

from platform import system

import matplotlib.pyplot as plt

class cliPlotPanel():
    def __init__(self):
        pass
        
    def clear(self, *args):
        plt.close(*args )
        
    def legend(self, *args, **kwargs):
        plt.legend(*args, **kwargs)
        
    def maximize(self, _flag=True):
        cfm=plt.get_current_fig_manager()
        try:
            if system() == "Windows":
                cfm.window.state("zoomed")  # This is windows only
            else:
                cfm.resize(*cfm.window.maxsize())
        except: # pylint: disable=bare-except
            pass
        
    def grid(self,*args):
        plt.grid(*args)
        
    def axis(self,*args):
        plt.axis(*args)
        
    def title(self,value):
        plt.title(value)
        
    def xlabel(self,value):
        plt.xlabel(value)
        
    def ylabel(self,value):
        plt.ylabel(value)
        
    def invert_yaxis(self):
        ax = plt.gca()
        ax.invert_yaxis()     
       
    def add_subplot(self,*args,**kwargs):
        return plt.subplot(*args,**kwargs)
            
    def view_info(self,value=True):
        pass
            
    def draw(self):
        plt.draw()

    def imshow(self, *args,**kwarg):
        plt.imshow(*args,**kwarg)
        
    def scatter(self,*args,**kwarg):
        plt.scatter(*args,**kwarg)
        
    def plot(self,*args,**kwarg):
        plt.plot(*args,**kwarg)
        
    def add_patch(self,*args,**kwargs):
        ax = plt.gca()
        ax.add_patch(*args,**kwargs)

    def ginput(self,*args,**kwarg):
        return plt.ginput(*args,**kwarg)
           
    def pause(self):
        print("to continue, close the plot window")
        plt.show()
        