#! /usr/bin/env python

# -*- coding: UTF-8 -*-

# ------------------------------------------------------------------------------
# Project: solarscan ( Solar Scan processing )
#
# This script processes the video solar acquisition of the sun made with the
# instrument Sol'Ex (http://www.astrosurf.com/solex/).
#
# ------------------------------------------------------------------------------
#    Author:  M27trognondepomme <pebe92 (at) gmail.com>
#
# This program is provided without any guarantee.
#
# The license is  LGPL-v3
# For details, see GNU General Public License, version 3 or later.
#                        "https://www.gnu.org/licenses/gpl.html"
# ------------------------------------------------------------------------------

import os
from solarscan.lib import tools #@UnresolvedImport

class CPrefs :
    def __init__(self, dirname, filename):
        self.filenamerc = filename
        self.dirnamerc  = dirname
        tools.mkdirs(self.dirnamerc)
        self.Default()

    def GetDir(self):
        return self.dirnamerc

    def Get(self, field ):
        return self.data[field]

    def Set(self, field, value ):
        self.data[field] = value

    def Default(self):
        self.data = {}
        self.data['workdir']       = "."
        self.data['currentvideo']  = ""
        self.data['debug']         = False        
        self.data['clahe']         = { 'clip': 0.8, 'tile_size':2 }
        self.data['high-contrast'] = { 'percent-high': 99.999, 'coef-low':0.15 }
        self.data['protuberance']  = { 'percent-high': 99.999, 'coef-high':0.5 }

    def Load(self,filename=None):
        if filename is None : filename = os.path.join(self.dirnamerc , self.filenamerc)
        if not os.path.isfile(filename) : return
        with open(filename,"r", encoding="utf-8") as fd :
            lines= fd.readlines()
            self.Default()
            try:
                prefs = tools.String2data(lines)
                for key in self.data.keys():
                    # ne charge que les cles existantes dans self.data()
                    if not ( key in prefs ):
                        continue # la cle n'existe pas, on passe a la suite
                    self.data[key]=prefs[key]
            except Exception as e : # pylint: disable=broad-except
                print("*** CPrefs::Load() error loading "+ filename + " :: " + str(e) )

    def Save(self,filename=None):
        if filename is None : filename = os.path.join(self.dirnamerc , self.filenamerc)
        chaine = tools.Stringify(self.data)
        with open(filename,"w", encoding="utf-8") as fd :
            fd.write("# Don't edit the file \n" )
            fd.write(chaine)
