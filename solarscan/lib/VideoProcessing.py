#! /usr/bin/env python

# -*- coding: UTF-8 -*-

# ------------------------------------------------------------------------------
# Project: solarscan ( Solar Scan processing )
#
# This script processes the video solar acquisition of the sun made with the
# instrument Sol'Ex (http://www.astrosurf.com/solex/).
#
# ------------------------------------------------------------------------------
#    Author:  M27trognondepomme <pebe92 (at) gmail.com>
#
# This program is provided without any guarantee.
#
# The license is  LGPL-v3
# For details, see GNU General Public License, version 3 or later.
#                        "https://www.gnu.org/licenses/gpl.html"
# ------------------------------------------------------------------------------

# ==============================================================================
class VideoProcessing():
    def __init__(self, log , iSolarTools, prefs):
        
        self.log=log
        self.iSolarTools=iSolarTools
        self.raw_filename=None
        
        self.Clear()
        self.clahe=prefs.Get('clahe')
        self.hc=prefs.data['high-contrast']
        self.protu=prefs.data['protuberance']
        
    def update_prefs(self,prefs):
        self.clahe=prefs.Get('clahe')
        self.hc=prefs.data['high-contrast']
        self.protu=prefs.data['protuberance']

    def Clear(self):
        self.raw_filename=None
        self.clahe={ 'clip': 0.8, 'tile_size':2 }
        
    def GetSelectedImage(self):
        return self.iSolarTools.GetBest()
    
    def GetRawFile(self):
        return self.raw_filename
    
    def GetParamClahe(self):
        return self.clahe
    
    def GetParamHighContrast(self):
        return self.hc
    
    def GetParamProtuberance(self):
        return self.protu
    
    # --------------------------------------------------------------------------
    def CheckSer(self):
        log = self.log
        iSolarTools=self.iSolarTools
        
        if iSolarTools.GetSerFd() == None :
            log.text( "***" , _("'Checking SER' Processing is Failed"), ": iSolarTools.GetSerFd()"  )
            iSolarTools.ViewInfo()
            return True
        return False
    # --------------------------------------------------------------------------
    def run_all(self, mode=0):
        log = self.log
        iSolarTools=self.iSolarTools
        
        nb_step=100/5
        iSolarTools.ViewInfo()
        
        if self.CheckSer() : return
        
        if  mode>2 or mode <0 :
            log.titre( "***" , _("Video Processing is aborted"), ": ", _("unknown mode") )
            iSolarTools.ViewInfo()
            return
        
        log.SetProgressBar(0*nb_step)     
          
        no_selected_image = iSolarTools.SearchingBestImage(mode)
        if no_selected_image == -1 :
            log.titre( "***" , _("Video Processing is aborted"), ": iSolarTools.SearchingBestImage()" )
            iSolarTools.ViewInfo()
            return
        log.SetProgressBar(1*nb_step)      

        if mode==0 :
            ymod = iSolarTools.SpectrumDistorting(no_selected_image,mode)
            if ymod == None :
                log.titre( "***" , _("Video Processing is aborted"), ": iSolarTools.SpectrumDistorting()" )
                iSolarTools.ViewInfo()
                return
        else :
            ymod = iSolarTools.SpectrumDistortingAuto(mode)
            if ymod == None :
                log.titre( "***" , _("Video Processing is aborted"), ": iSolarTools.SpectrumDistortingAuto()" )
                iSolarTools.ViewInfo()
                return

        log.SetProgressBar(2*nb_step)  
             
        self.raw_filename = iSolarTools.SlitCorrectionProcess( no_selected_image, ymod, mode)
        if self.raw_filename == None :
            log.titre( "***" , _("Video Processing is aborted"), ": iSolarTools.CorrectionProcess()" )
            iSolarTools.ViewInfo()
            return
        log.SetProgressBar(3*nb_step)       
        
                
        raw_filename_tr=iSolarTools.TransversalliumCorrection(self.raw_filename, mode )
        if raw_filename_tr == None :
            iSolarTools.ViewInfo()
            return
        log.SetProgressBar(4*nb_step)       
 
        filename_sun=iSolarTools.ReshapeRawSun(raw_filename_tr, mode )
        if filename_sun == None :
            iSolarTools.ViewInfo()
            return
        
        filename_clahe=iSolarTools.ClaheProcessing(filename_sun, self.clahe['clip'], self.clahe['tile_size'],mode)
        if filename_clahe == None :
            iSolarTools.ViewInfo()
            return
        
        filename_hc=iSolarTools.HighContrastProcessing(filename_sun, self.hc['coef-low'], self.hc['percent-high'] )
        if filename_hc == None :
            iSolarTools.ViewInfo()
            return
        
        filename_protu=iSolarTools.ProtuberanceProcessing(filename_sun, self.protu['coef-high'], self.protu['percent-high'] )
        if filename_protu == None :
            iSolarTools.ViewInfo()
            return
        
        iSolarTools.plot_final(filename_sun,filename_clahe,filename_hc,filename_protu)
        
        log.SetFinishBar()

        iSolarTools.ViewImage()

        log.ligne('-')
        log.titre( "-" , _("Video Processing is finished") )

    # --------------------------------------------------------------------------
    def SearchingBestImage(self):
        log = self.log
        iSolarTools=self.iSolarTools
        
        if self.CheckSer() : return

        no_selected_image = iSolarTools.SearchingBestImage(0)
        if no_selected_image == -1 :
            log.titre( "***" , _("'Best Position' Processing  is aborted"), ": iSolarTools.SearchingBestImage()" )
            iSolarTools.ViewInfo()
            return
        
        iSolarTools.ViewImage()
        log.ligne('-')
        log.titre( "-" , _("Best Position is finished") )

    # --------------------------------------------------------------------------
    def SpectrumDistorting(self, number):
        log = self.log
        iSolarTools=self.iSolarTools
                
        if self.CheckSer() : return

        ymod = iSolarTools.SpectrumDistorting(number,0)
        if ymod == None :
            log.titre( "***" , _("'Spectrum Distorting' is aborted"), ": iSolarTools.SpectrumDistorting()" )
            iSolarTools.ViewInfo()
            return
        
        iSolarTools.ViewImage()
        log.ligne('-')
        log.titre( "-" , _("'Spectrum Distorting' Processing is finished") )

    # --------------------------------------------------------------------------
    def SpectrumDistortingAuto(self):
        log = self.log
        iSolarTools=self.iSolarTools
                
        if self.CheckSer() : return

        ymod = iSolarTools.SpectrumDistortingAuto(0)
        if ymod == None :
            log.titre( "***" , _("'Auto Spectrum Distorting' is aborted"), ": iSolarTools.SpectrumDistorting()" )
            iSolarTools.ViewInfo()
            return
                            
        iSolarTools.ViewImage()
        log.ligne('-')
        log.titre( "-" , _("'Auto Spectrum Distorting' Processing is finished") )

    # --------------------------------------------------------------------------
    def RawSunImage(self):
        log = self.log
        iSolarTools=self.iSolarTools
                
        if self.CheckSer() : return

        self.raw_filename = iSolarTools.SlitCorrectionProcess( iSolarTools.GetBest(), iSolarTools.GetYmod(), mode=0)
        if self.raw_filename == None :
            log.titre( "***" , _("'Raw Sun Image' is aborted"), ": iSolarTools.CorrectionProcess()" )
            iSolarTools.ViewInfo()
            return
        
        iSolarTools.ViewImage()
        log.ligne('-')
        log.titre( "-" , _("'Raw Sun Image' Processing is finished") )

    # --------------------------------------------------------------------------
    def ReshapeSunImage(self,raw_filename=None,mode=1):
        log = self.log
        iSolarTools=self.iSolarTools
                
        if raw_filename is None : raw_filename = self.raw_filename
                
        ret=iSolarTools.ReshapeRawSun(raw_filename, mode)
        if ret == None :
            iSolarTools.ViewInfo()
            return
        
        iSolarTools.ViewImage()
        log.ligne('-')
        log.titre( "-" , _("'Reshape Sun Image' Processing is finished") )

    # --------------------------------------------------------------------------
    def TransversalliumCorrection(self,raw_filename=None):
        log = self.log
        iSolarTools=self.iSolarTools
                
        if raw_filename is None : raw_filename = self.raw_filename
                
        ret=iSolarTools.TransversalliumCorrection(raw_filename, 0 )
        if ret == None :
            iSolarTools.ViewInfo()
            return
        
        iSolarTools.ViewImage()
        log.ligne('-')
        log.titre( "-" , _("'Transversallium Correction' Processing is finished") )

    # --------------------------------------------------------------------------
    def ClaheProcessing(self,sun_filename, clip=2,tile_sz=8):
        log = self.log
        iSolarTools=self.iSolarTools
                
        ret=iSolarTools.ClaheProcessing(sun_filename, clip, tile_sz, 0)
        if ret == None :
            iSolarTools.ViewInfo()
            return
        self.clahe['clip']=clip
        self.clahe['tile_size']=tile_sz
        
        iSolarTools.ViewImage()
        log.ligne('-')
        log.titre( "-" , _("'CLAHE' Processing is finished") )
        
    # --------------------------------------------------------------------------
    def HighContrastProcessing(self,sun_filename, coef_low=0.15, percent_high=99.999):
        log = self.log
        iSolarTools=self.iSolarTools
                
        ret=iSolarTools.HighContrastProcessing(sun_filename, coef_low, percent_high, 0)
        if ret == None :
            iSolarTools.ViewInfo()
            return
        self.hc['coef-low']=coef_low
        self.hc['percent-high']=percent_high
        
        iSolarTools.ViewImage()
        log.ligne('-')
        log.titre( "-" , _("'High Contrast' Processing is finished") )
        
    # --------------------------------------------------------------------------
    def ProtuberanceProcessing(self,sun_filename, coef_high=0.5, percent_high=99.999):
        log = self.log
        iSolarTools=self.iSolarTools
                
        ret=iSolarTools.ProtuberanceProcessing(sun_filename, coef_high, percent_high, 0)
        if ret == None :
            iSolarTools.ViewInfo()
            return
        self.protu['coef-high']=coef_high
        self.protu['percent-high']=percent_high
        
        iSolarTools.ViewImage()
        log.ligne('-')
        log.titre( "-" , _("'Protuberance' Processing is finished") )
        
    # --------------------------------------------------------------------------
    def Test(self,raw_filename=None):
        log = self.log
        iSolarTools=self.iSolarTools
                
        if raw_filename is None : raw_filename = self.raw_filename
                
        ret=iSolarTools.SignalNoiseImage(raw_filename, 0 )
        if ret == None :
            iSolarTools.ViewInfo()
            return
        
        iSolarTools.ViewImage()
        log.ligne('-')
        log.titre( "-" , _("'Test' Processing is finished") )

