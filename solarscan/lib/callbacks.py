#! /usr/bin/env python

# -*- coding: UTF-8 -*-

# ------------------------------------------------------------------------------
# Project: solarscan ( Solar Scan processing )
#
# This script processes the video solar acquisition of the sun made with the
# instrument Sol'Ex (http://www.astrosurf.com/solex/).
#
# ------------------------------------------------------------------------------
#    Author:  M27trognondepomme <pebe92 (at) gmail.com>
#
# This program is provided without any guarantee.
#
# The license is  LGPL-v3
# For details, see GNU General Public License, version 3 or later.
#                        "https://www.gnu.org/licenses/gpl.html"
# ------------------------------------------------------------------------------

import sys
import os
import wx.adv
import cv2
from solarscan.lib  import changelog,tlog,VideoProcessing #@UnresolvedImport
from solarscan.lib  import solartools,wxPlotPanel,tools   #@UnresolvedImport
from solarscan.ui   import gui                            #@UnresolvedImport

# ==============================================================================
class CCallbacks():
    def __init__(self, i_gui, i_prefs , homedir ):
        self.th_run      = None
        self.i_gui       = i_gui
        self.prefs       = i_prefs

        self.prefs.Load()

        self.log  = tlog.LogConsole(self.i_gui, i_gui.tLog,self.i_gui.gProgress)
        self.log.LogTraceMain(True,homedir)

        self.plot = wxPlotPanel.wxPlotPanel(i_gui.notebook_1, wx.ID_ANY)
        i_gui.notebook_1.AddPage(self.plot, _("Image"))

        self.iSolarTools= solartools.solartools(self.log, self.plot,self.prefs )
        self.vp = VideoProcessing.VideoProcessing(self.log,self.iSolarTools,self.prefs)

        self.BaseTitle  = i_gui.GetTitle()
        self.mem_stdout = sys.stdout
        self.mem_stderr = sys.stderr
        self.fd_stddbg  = None
        self.DebugTrace(self.prefs.Get('debug'))

        self.CurrentVideo      = self.prefs.Get('currentvideo')
        self.bExitWin          = False


        i_gui.Bind(wx.EVT_CLOSE, self.CB_closed )
        i_gui.Bind(wx.EVT_MENU,  self.CB_closed , id=wx.ID_EXIT )


        # bind with callback
        i_gui.Bind(wx.EVT_MENU,  self.CB_OpenVideo          , id=self.i_gui.SolarScan_menubar.mLoad.GetId() )
        i_gui.Bind(wx.EVT_MENU,  self.CB_OpenImage          , id=self.i_gui.SolarScan_menubar.mOpenImage.GetId() )
        i_gui.Bind(wx.EVT_MENU,  lambda event, mode=0: self.CB_ProcessVideo(event,mode) , id=self.i_gui.SolarScan_menubar.mProcessManual.GetId())
        i_gui.Bind(wx.EVT_MENU,  lambda event, mode=1: self.CB_ProcessVideo(event,mode) , id=self.i_gui.SolarScan_menubar.mProcessSemiAuto.GetId())
        i_gui.Bind(wx.EVT_MENU,  lambda event, mode=2: self.CB_ProcessVideo(event,mode) , id=self.i_gui.SolarScan_menubar.mProcessAuto.GetId())
        i_gui.Bind(wx.EVT_MENU,  self.CB_BestPosition       , id=self.i_gui.SolarScan_menubar.mBestPosition.GetId())
        i_gui.Bind(wx.EVT_MENU,  self.CB_SpectrumDistorting , id=self.i_gui.SolarScan_menubar.mSpectrumDistorting.GetId())
        i_gui.Bind(wx.EVT_MENU,  self.CB_SpectrumDistortingAuto , id=self.i_gui.SolarScan_menubar.mSpectrumDistortingAuto.GetId())
        i_gui.Bind(wx.EVT_MENU,  self.CB_RawSunImage        , id=self.i_gui.SolarScan_menubar.mRawSunImage.GetId())
        i_gui.Bind(wx.EVT_MENU,  self.CB_Transversallium    , id=self.i_gui.SolarScan_menubar.mTransversallium.GetId())
        i_gui.Bind(wx.EVT_MENU,  lambda event, mode=0: self.CB_ReshapeSunImage(event,mode) , id=self.i_gui.SolarScan_menubar.mReshapeSunImage.GetId())
        i_gui.Bind(wx.EVT_MENU,  lambda event, mode=1: self.CB_ReshapeSunImage(event,mode) , id=self.i_gui.SolarScan_menubar.mReshapeSunImageAuto.GetId())
        i_gui.Bind(wx.EVT_MENU,  self.CB_CLAHE              , id=self.i_gui.SolarScan_menubar.mClahe.GetId())
        i_gui.Bind(wx.EVT_MENU,  self.CB_HighContrast       , id=self.i_gui.SolarScan_menubar.mHighContrast.GetId())
        i_gui.Bind(wx.EVT_MENU,  self.CB_Protuberance       , id=self.i_gui.SolarScan_menubar.mProtuberance.GetId())
        i_gui.Bind(wx.EVT_MENU,  self.CB_Test               , id=self.i_gui.SolarScan_menubar.mTest.GetId())

        i_gui.Bind(wx.EVT_MENU,  self.CB_Prefs              , id=self.i_gui.SolarScan_menubar.mPrefs.GetId() )
        i_gui.Bind(wx.EVT_MENU,  self.CB_About              , id=self.i_gui.SolarScan_menubar.mAbout.GetId() )

        self.i_gui.SetAcceleratorTable(wx.AcceleratorTable([
                        (wx.ACCEL_NORMAL, wx.WXK_F1, wx.ID_EXIT ),
                        ]))

        self.log.LogTraceImage(True,self.CurrentVideo)
        if self.iSolarTools.OpenSer(self.CurrentVideo) != None :
            self.iSolarTools.LoadData()
            self.iSolarTools.DisplayInfo()

        self.SetUpdateTitle( self.CurrentVideo )

    # --------------------------------------------------------------------------
    def SetUpdateTitle(self, videoname ):
        if len(videoname)==0 :
            self.i_gui.SetTitle(self.BaseTitle)
        else:
            self.i_gui.SetTitle(self.BaseTitle + "           *** " + _("solar video") + " : " + os.path.basename(videoname)  + " ***")


    # --------------------------------------------------------------------------
    def CB_closed(self,_event): 
        self.iSolarTools.SaveData()
        self.DebugTrace(False)
        self.bExitWin = True
        self.prefs.Save()
        self.log.LogTraceMain(False)
        self.log.LogTraceImage(False)
        self.i_gui.Destroy()
        return self.prefs

    # --------------------------------------------------------------------------
    def CB_OpenVideo(self, _event):
        typefile="Video files |*.ser"
        flag    = wx.FD_OPEN | wx.FD_FILE_MUST_EXIST
        with wx.FileDialog(None, "Open", self.prefs.Get('workdir'), "",typefile , flag) as openFileDialog :
            if openFileDialog.ShowModal() == wx.ID_CANCEL:
                return
            else :
                self.CurrentVideo = openFileDialog.GetPath()
                self.prefs.Set('workdir', os.path.dirname(self.CurrentVideo))
                self.prefs.Set('currentvideo',self.CurrentVideo)
                self.log.LogTraceImage(True,self.CurrentVideo)

        self.iSolarTools.LoadData()
        if self.iSolarTools.OpenSer(self.CurrentVideo) != None :
            self.vp.Clear()
            self.iSolarTools.DisplayInfo()
        else:
            self.CurrentVideo= ""

        self.SetUpdateTitle( self.CurrentVideo )

    # --------------------------------------------------------------------------
    def CB_OpenImage(self, _event):
        typefile="Image files |*.png"
        flag    = wx.FD_OPEN | wx.FD_FILE_MUST_EXIST
        with wx.FileDialog(None, "Open", self.prefs.Get('workdir'), "",typefile , flag) as openFileDialog :
            if openFileDialog.ShowModal() == wx.ID_OK:
                filename=openFileDialog.GetPath()
                image=cv2.imread(filename, cv2.IMREAD_ANYDEPTH )
                splt=self.plot
                splt.clear('all')
                splt.add_subplot()
                splt.imshow(image,cmap='gray' )
                splt.title(os.path.basename(filename))
                splt.draw()
                self.plot.view_info(False)

    # --------------------------------------------------------------------------
    def CB_ProcessVideo(self, _event, mode): 
        self.vp.run_all(mode)
        
    # --------------------------------------------------------------------------
    def CB_BestPosition(self, _event): 
        self.vp.SearchingBestImage()
        
    # --------------------------------------------------------------------------
    def CB_SpectrumDistorting(self, _event):
        nbFrame=self.iSolarTools.GetNumberFrame()
        if nbFrame== -1 : return
        number=self.vp.GetSelectedImage()
        if number == -1 : number=int(nbFrame/2)
        dlg=wx.NumberEntryDialog(None, _("frame number"), "", _("frame number entry"), number , 0, nbFrame)
        if dlg.ShowModal() == wx.ID_OK:
            self.vp.SpectrumDistorting(int(dlg.GetValue()))
        else:
            self.log.text("*** " + _("Cancelled"))
        dlg.Destroy()

    # --------------------------------------------------------------------------
    def CB_SpectrumDistortingAuto(self, _event):
        self.vp.SpectrumDistortingAuto()

    # --------------------------------------------------------------------------
    def CB_RawSunImage(self, _event):
        self.vp.RawSunImage()

    # --------------------------------------------------------------------------
    def CB_ReshapeSunImage(self, _event, mode): 
        typefile="raw files |*.png"
        flag    = wx.FD_OPEN | wx.FD_FILE_MUST_EXIST
        dossier = self.iSolarTools.GetDirOut()
        with wx.FileDialog(None, "Open", dossier , "",typefile , flag) as openFileDialog :
            if openFileDialog.ShowModal() == wx.ID_CANCEL:
                self.log.text("*** " + _("Cancelled"))
                return
            else :
                raw_filename = openFileDialog.GetPath()
                self.vp.ReshapeSunImage(raw_filename=raw_filename,mode=mode)

    # --------------------------------------------------------------------------
    def CB_Transversallium(self, _event):
        typefile="raw files |*.png"
        flag    = wx.FD_OPEN | wx.FD_FILE_MUST_EXIST
        dossier = self.iSolarTools.GetDirOut()
        with wx.FileDialog(None, "Open", dossier, "",typefile , flag) as openFileDialog :
            if openFileDialog.ShowModal() == wx.ID_CANCEL:
                self.log.text("*** " + _("Cancelled"))
                return
            else :
                raw_filename = openFileDialog.GetPath()
                self.vp.TransversalliumCorrection(raw_filename)

    # --------------------------------------------------------------------------
    def CB_HighContrast(self, _event):
        typefile="sun files |*.png"
        flag    = wx.FD_OPEN | wx.FD_FILE_MUST_EXIST
        dossier = self.iSolarTools.GetDirOut()
        with wx.FileDialog(None, "Open", dossier, "",typefile , flag) as openFileDialog :
            if openFileDialog.ShowModal() == wx.ID_CANCEL:
                self.log.text("*** " + _("Cancelled"))
                return
            else :
                sun_filename = openFileDialog.GetPath()                
                with gui.CHighContrastDlg(None, -1, "") as dlg :
                    hc=self.vp.GetParamHighContrast()
                    dlg.spPercentHigh_HC.SetValue(hc['percent-high'])
                    dlg.spCoefLow_HC.SetValue(hc['coef-low'])        
                    if dlg.ShowModal() == wx.ID_CANCEL: return
                    hc['percent-high']=dlg.spPercentHigh_HC.GetValue()
                    hc['coef-low']=dlg.spCoefLow_HC.GetValue()
                    self.vp.HighContrastProcessing(sun_filename,hc['coef-low'],hc['percent-high'])
                    self.prefs.Set('clahe',self.vp.GetParamClahe())
       
    # --------------------------------------------------------------------------
    def CB_Protuberance(self, _event):
        typefile="sun files |*.png"
        flag    = wx.FD_OPEN | wx.FD_FILE_MUST_EXIST
        dossier = self.iSolarTools.GetDirOut()
        with wx.FileDialog(None, "Open", dossier, "",typefile , flag) as openFileDialog :
            if openFileDialog.ShowModal() == wx.ID_CANCEL:
                self.log.text("*** " + _("Cancelled"))
                return
            else :
                sun_filename = openFileDialog.GetPath()            
                with gui.CProtuberanceDlg(None, -1, "") as dlg :
                    protu=self.vp.GetParamProtuberance()
                    dlg.spPercentHigh.SetValue(protu['percent-high'])
                    dlg.spCoefHigh.SetValue(protu['coef-high'])        
                    if dlg.ShowModal() == wx.ID_CANCEL: return
                    protu['percent-high']=dlg.spPercentHigh.GetValue()
                    protu['coef-high']=dlg.spCoefHigh.GetValue()
                    self.vp.ProtuberanceProcessing(sun_filename,protu['coef-high'],protu['percent-high'])
                    self.prefs.Set('protuberance',self.vp.GetParamProtuberance())
        
    # --------------------------------------------------------------------------
    def CB_CLAHE(self, _event):
        typefile="sun files |*.png"
        flag    = wx.FD_OPEN | wx.FD_FILE_MUST_EXIST
        dossier = self.iSolarTools.GetDirOut()
        with wx.FileDialog(None, "Open", dossier, "",typefile , flag) as openFileDialog :
            if openFileDialog.ShowModal() == wx.ID_CANCEL:
                self.log.text("*** " + _("Cancelled"))
                return
            else :
                sun_filename = openFileDialog.GetPath()
                
                with gui.CCLaheDialog(None, -1, "") as dlg :
                    clahe=self.vp.GetParamClahe()
                    dlg.spClip.SetValue(clahe['clip'])
                    dlg.spTile.SetValue(clahe['tile_size'])        
                    if dlg.ShowModal() == wx.ID_CANCEL: return
                    clahe['clip']=dlg.spClip.GetValue()
                    clahe['tile_size']=dlg.spTile.GetValue()
                    self.vp.ClaheProcessing(sun_filename,clahe['clip'],clahe['tile_size'])
                    self.prefs.Set('clahe',self.vp.GetParamClahe())

    # --------------------------------------------------------------------------
    def CB_Test(self, _event):
        typefile="raw files |*.png"
        flag    = wx.FD_OPEN | wx.FD_FILE_MUST_EXIST
        dossier = self.iSolarTools.GetDirOut()
        with wx.FileDialog(None, "Open", dossier, "",typefile , flag) as openFileDialog :
            if openFileDialog.ShowModal() == wx.ID_CANCEL:
                self.log.text("*** " + _("Cancelled"))
                return
            else :
                raw_filename = openFileDialog.GetPath()
                self.vp.Test(raw_filename)

    # --------------------------------------------------------------------------
    def CB_Prefs(self, _event): 
        with gui.CPrefsDlg(None, -1, "") as dlg : 
            dlg.spClip.SetValue(  self.prefs.Get('clahe')['clip'] )
            dlg.spTile.SetValue(  self.prefs.Get('clahe')['tile_size'] )
            
            dlg.spPercentHigh_HC.SetValue( self.prefs.Get('high-contrast')['percent-high'] )
            dlg.spCoefLow_HC.SetValue(     self.prefs.Get('high-contrast')['coef-low'] )
            
            dlg.spPercentHigh.SetValue( self.prefs.Get('protuberance')['percent-high'] )
            dlg.spCoefHigh.SetValue(     self.prefs.Get('protuberance')['coef-high'] )
            
            if dlg.ShowModal() == wx.ID_CANCEL: return
            
            self.prefs.Set('clahe', {'clip':dlg.spClip.GetValue(), 'tile_size':dlg.spTile.GetValue()})
            self.prefs.Set('high-contrast', {'percent-high':dlg.spPercentHigh_HC.GetValue(), 'coef-low':dlg.spCoefLow_HC.GetValue()})
            self.prefs.Set('protuberance', {'percent-high':dlg.spPercentHigh.GetValue(), 'coef-high':dlg.spCoefHigh.GetValue()})
            self.vp.update_prefs(self.prefs)

    def DebugTrace(self, enable ) :
        if enable :
            try:
                self.fd_stddbg=open(os.path.join( self.prefs.Get('workdir'), "solarscan-trace.log" ),"w", encoding="utf-8")
                sys.stdout = self.fd_stddbg
                sys.stderr = self.fd_stddbg
            except: # pylint: disable=bare-except
                self.fd_stddbg = None
        else:
            if self.fd_stddbg is not None :
                sys.stdout = self.mem_stdout
                sys.stderr = self.mem_stderr
                self.fd_stddbg.close()
                self.fd_stddbg = None

    # --------------------------------------------------------------------------
    def CB_About(self, _event) :
        description= _("""
SOLARSCAN ( SOLAR ) is a software for processing
the video acquisition of the sun made with the
instrument [Sol'Ex] (http://www.astrosurf.com/solex/).

It does three things:

1. Reading of the sequence
2. Reconstruction of the raw solar disk
3. Geometric correction of the solar disk

A user manual is available here :
[wiki solarscan](https://gitlab.com/free-astro/solarscan/-/wikis/home)

""" )  + changelog.CHANGELOG
        licence = """
This program is provided without any guarantee.
The license is  LGPL-v3
For details, see GNU General Public License, version 3 or later.
"https://www.gnu.org/licenses/gpl.html"

"""

        info = wx.adv.AboutDialogInfo()

        iconname     = os.path.join("..","icon","solarscan.png")
        iconpath     = os.path.dirname(os.path.abspath(__file__))
        iconfullname = tools.resource_path(iconname, pathabs=iconpath )

        info.SetIcon(wx.Icon(iconfullname, wx.BITMAP_TYPE_PNG))
        info.SetName('Solar Scan')
        info.SetVersion(changelog.NO_VERSION)
        info.SetDescription(description)
        info.SetCopyright('(C) ' +  changelog.DATE_VERSION + ' M27trognondepomme')
        info.SetWebSite('https://gitlab.com/free-astro/pysolarscan')
        info.SetLicence(licence)
        info.AddDeveloper('M27trognondepomme')
        info.AddDocWriter('M27trognondepomme')
        #info.AddArtist('The Tango crew')
        info.AddTranslator('M27trognondepomme')

        wx.adv.AboutBox(info)