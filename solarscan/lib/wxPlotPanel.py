#! /usr/bin/env python

# -*- coding: UTF-8 -*-

# ------------------------------------------------------------------------------
# Project: solarscan ( Solar Scan processing )
#
# This script processes the video solar acquisition of the sun made with the
# instrument Sol'Ex (http://www.astrosurf.com/solex/).
#
# ------------------------------------------------------------------------------
#    Author:  M27trognondepomme <pebe92 (at) gmail.com>
#
# This program is provided without any guarantee.
#
# The license is  LGPL-v3
# For details, see GNU General Public License, version 3 or later.
#                        "https://www.gnu.org/licenses/gpl.html"
# ------------------------------------------------------------------------------

import wx

from matplotlib.backends.backend_wxagg import (
    FigureCanvasWxAgg       as FigureCanvas,
    NavigationToolbar2WxAgg as NavigationToolbar,
)

import matplotlib as mpl
#import matplotlib.pyplot as plt 

class wxPlotPanel(wx.Panel):
    def __init__(self,parent,wid):
        super().__init__(parent, wid )

        self.nb=parent
        self.fig = mpl.figure.Figure()
        
        self.add_subplot()

        self.canvas = FigureCanvas(self, -1, self.fig)
        self.toolbar = NavigationToolbar(self.canvas)
        self.toolbar.Realize()

        self.sizer = wx.BoxSizer(wx.VERTICAL)
        self.sizer.Add(self.canvas, 1,  wx.EXPAND)
        self.sizer.Add(self.toolbar, 0, wx.LEFT | wx.EXPAND)
        self.SetSizer(self.sizer)
        self.Fit()
        self.splot = None
        
    def axes(self, number=None):
        if number == None :
            return self.fig.get_axes()
        self.splot=self.fig.get_axes()[number]
        
    def clear(self, number=-1):
        if number == 'all' :
            for splot in self.fig.get_axes() : 
                splot.clear()
                splot.remove()
                self.fig.canvas.draw()
            return
        splot=self.fig.get_axes()[number]
        splot.clear()
        splot.remove()
        self.fig.canvas.draw()
        
    def legend(self, number=-1):
        splot=self.fig.get_axes()[number]
        splot.legend()
        
    def maximize(self, flag=True):
        self.GetTopLevelParent().Maximize(flag)
        
    def grid(self,*args):
        splot=self.fig.get_axes()[-1]
        splot.grid(*args)
        
    def axis(self,*args):
        splot=self.fig.get_axes()[-1]
        splot.axis(*args)
        
    def title(self,value):
        splot=self.fig.get_axes()[-1]
        splot.set_title(value)
        
    def xlabel(self,value):
        splot=self.fig.get_axes()[-1]
        splot.set_xlabel(value)
        
    def ylabel(self,value):
        splot=self.fig.get_axes()[-1]
        splot.set_ylabel(value)
        
    def invert_yaxis(self):
        splot=self.fig.get_axes()[-1]
        splot.invert_yaxis()     
       
    def add_subplot(self,*args,**kwargs):
        return self.fig.add_subplot(*args,**kwargs)
    
    def add_patch(self,*args,**kwargs):
        splot=self.fig.get_axes()[-1]
        splot.add_patch(*args,**kwargs)
            
    def view_info(self,value=True):
        if value :
            self.nb.ChangeSelection( 0 )
        else:
            self.nb.ChangeSelection( 1 )
            
    def draw(self):
        self.fig.canvas.draw()

    def imshow(self, *args,**kwarg):
        splot=self.fig.get_axes()[-1]
        splot.imshow(*args,**kwarg)
        
    def scatter(self,*args,**kwarg):
        splot=self.fig.get_axes()[-1]
        splot.scatter(*args,**kwarg)
        
    def plot(self,*args,**kwarg):
        splot=self.fig.get_axes()[-1]
        splot.plot(*args,**kwarg)
        
    def ginput(self,*args,**kwarg):
        return self.fig.ginput(*args,**kwarg)
           
    def pause(self):
        dlg=PauseDialog(None, wx.ID_ANY, size=(200,100),style=wx.DEFAULT_DIALOG_STYLE | wx.DIALOG_NO_PARENT)
        dlg.ShowModal()

class PauseDialog(wx.Dialog):

    def __init__(self, *args, **kw):
        super(PauseDialog, self).__init__(*args, **kw)
        self.InitUI()
        #self.Fit()
        self.Centre()

    def InitUI(self):
        vbox = wx.BoxSizer(wx.VERTICAL)        
        label=wx.StaticText(self, wx.ID_ANY, _("Pause"))
        hbox1 = wx.BoxSizer(wx.HORIZONTAL)
        okButton = wx.Button(self, label=_('Continue'))
        hbox1.Add(okButton)
        vbox.Add(label, proportion=0, flag=wx.ALIGN_CENTER, border=50)
        vbox.Add(hbox1, proportion=0, flag=wx.ALIGN_CENTER, border=10)
        self.SetSizer(vbox)
        self.Layout()
        okButton.Bind(wx.EVT_BUTTON, self.OnClose)

    def OnClose(self, _e):
        self.Destroy()
        