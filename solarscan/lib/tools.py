#! /usr/bin/env python

# -*- coding: UTF-8 -*-

# ------------------------------------------------------------------------------
# Project: solarscan ( Solar Scan processing )
#
# This script processes the video solar acquisition of the sun made with the 
# instrument Sol'Ex (http://www.astrosurf.com/solex/).
#
# ------------------------------------------------------------------------------
#    Author:  M27trognondepomme <pebe92 (at) gmail.com>
#
# This program is provided without any guarantee.
#
# The license is  LGPL-v3
# For details, see GNU General Public License, version 3 or later.
#                        "https://www.gnu.org/licenses/gpl.html"
# ------------------------------------------------------------------------------
import sys
import os
import gettext
import locale
import re
from ast import literal_eval

# ==============================================================================
def resource_path(relative_path, pathabs=None):
    """ Get absolute path to resource, works for dev and for PyInstaller """
    try:
        # PyInstaller creates a temp folder and stores path in _MEIPASS
        base_path =  getattr(sys, '_MEIPASS')
    except AttributeError : # pylint: disable=bare-except
        if pathabs== None :
            base_path = os.path.dirname( sys.argv[0] )
        else:
            base_path = pathabs

    return os.path.join(base_path, relative_path)

def init_language():
    try:
        if sys.platform.startswith('win'):
            if os.getenv('LANG') is None:
                lang, enc = locale.getdefaultlocale()
                os.environ['LANG'] = lang
                if len(enc) >0:  os.environ['LANG'] += "." + enc
        langue= os.environ['LANG'].split('.' )[0]
        if not re.search( "_" ,langue ) : langue = langue.lower() + "_" + langue.upper()
        path_abs=os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
        i18n_dir = resource_path( os.path.join('i18n' ,langue),pathabs=path_abs )

        if not os.path.exists(i18n_dir )  :
            gettext.install('solarscan')
        else:
            lang = gettext.translation('solarscan', os.path.dirname(i18n_dir) , languages=[langue,os.environ['LANG']] )
            lang.install()
    except Exception as e : # pylint: disable=broad-except
        print("error init_language():" + str(e))
        gettext.install('solarscan')

init_language()

# ==============================================================================
def Stringify(data,ident1=4,ident2=8):
    IDENT1 = " " * ident1
    IDENT2 = " " * ident2
    str_data=str(data).replace(',',',\n'+IDENT2)
    str_data=str_data.replace('{',IDENT1+'{\n '+IDENT2)
    str_data=str_data.replace('}','\n'+IDENT1+'}')
    return str_data

# ==============================================================================
def String2data( lines ) :
    str_data=""
    for chaine in lines:
        chaine=chaine.lstrip()
        chaine=chaine.rstrip()
        if not chaine : continue
        if (chaine[0] == "#" ) : continue # skip comments
        str_data= str_data + chaine
    return literal_eval(str_data)
# ==============================================================================
def path_conv( pathname ):
    return os.path.normpath( pathname )

# ==============================================================================
def mkdirs(name):
    path= path_conv(name)
    if not os.path.exists(path): os.makedirs(path)

