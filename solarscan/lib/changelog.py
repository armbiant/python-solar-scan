#! /usr/bin/env python

# -*- coding: UTF-8 -*-

# ------------------------------------------------------------------------------
# Project: solarscan ( Solar Scan processing )
#
# This script processes the video solar acquisition of the sun made with the
# instrument Sol'Ex (http://www.astrosurf.com/solex/).
#
# ------------------------------------------------------------------------------
#    Author:  M27trognondepomme <pebe92 (at) gmail.com>
#
# This program is provided without any guarantee.
#
# The license is  LGPL-v3
# For details, see GNU General Public License, version 3 or later.
#                        "https://www.gnu.org/licenses/gpl.html"
# ------------------------------------------------------------------------------

DATE_VERSION="October 2022"
NO_VERSION="0.0.3x"
CHANGELOG="""CHANGELOG:
    V""" + NO_VERSION + """
      + fix: circularize solar disk ( rotation, interpolation, derotation ...)
      + ...
    V0.0.2
      + add: display high contrast image, protuberance image, CLAHE image
      + add: high contrast image
      + add: protuberance image
      + add: manual solar disk selection 
      + add: if mode auto is failed then switch manual mode
      + fix: xzoom
      + fix: circularizing function
    V0.0.1
      + mode full automatic
    V0.0.0
      + initial
"""
