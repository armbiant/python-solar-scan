#! /usr/bin/env python

# -*- coding: UTF-8 -*-

# ------------------------------------------------------------------------------
# Project: solarscan ( Solar Scan processing )
#
# This script processes the video solar acquisition of the sun made with the
# instrument Sol'Ex (http://www.astrosurf.com/solex/).
#
# ------------------------------------------------------------------------------
#    Author:  M27trognondepomme <pebe92 (at) gmail.com>
#
# This program is provided without any guarantee.
#
# The license is  LGPL-v3
# For details, see GNU General Public License, version 3 or later.
#                        "https://www.gnu.org/licenses/gpl.html"
# ------------------------------------------------------------------------------
try:
    import wx
except: # pylint: disable=bare-except
    pass
import os
import re
from solarscan.lib import tools      #@UnresolvedImport
from solarscan.lib import constantes #@UnresolvedImport

# ==============================================================================
class LogConsole:

    def __init__(self, uilog, tlog,gBarreEnCours):
        self.uilog             = uilog
        self.tlog              = tlog
        self.progressBar       = gBarreEnCours

        self.fdlogMain = None
        self.fdlogImg = None
        if uilog == None or tlog == None :
            self.uilog = None
            self.tlog  = None
            self.progressBar = None
        else:
            font = wx.Font(10, wx.MODERN, wx.NORMAL, wx.NORMAL, False, 'Console')
            self.style=[]
            self.style.append( wx.TextAttr("black"    ,"white", font       ) )
            self.style.append( wx.TextAttr("#008080"  ,"white", font.Bold()) )
            self.style.append( wx.TextAttr("maroon"   ,"white", font.Bold()) )
            self.style.append( wx.TextAttr("red"      ,"white", font.Bold()) )

            self.uilog.bclear.Bind(wx.EVT_BUTTON      , self.CB_clearlog )
            self.uilog.bsearchnext.Bind(wx.EVT_BUTTON , self.CB_searchnext )
            self.uilog.bsearchprev.Bind(wx.EVT_BUTTON , self.CB_searchprev )
            self.progressBar.SetRange(100)

        self.translation = "".maketrans("������","eeecau")

        self.oldpos=-1
        self.wordlen=0
        self.stepNumber = 1

    def CB_clearlog(self, _event=None):
        self.oldpos=-1
        self.wordlen=0
        self.clear()

    def CB_searchnext(self, _event=None):
        chaine   = self.uilog.esearch.GetValue()
        if len(chaine)==0 : return
        self.unhighlightText(self.oldpos, self.wordlen )
        poscur    = self.tlog.GetInsertionPoint()
        pos_found = self.find_str(chaine,self.tlog.GetValue(), poscur, +1 )
        self.highlightText(pos_found,pos_found+len(chaine))

    def CB_searchprev(self, _event=None):
        chaine = self.uilog.esearch.GetValue()
        if len(chaine)==0 : return
        self.unhighlightText(self.oldpos, self.wordlen )
        poscur    = self.tlog.GetInsertionPoint()
        pos_found = self.find_str(chaine,self.tlog.GetValue(), poscur, -1 )
        self.highlightText(pos_found,pos_found+len(chaine) )

    def highlightText(self, pos, size):
        if pos<0     : return
        if size <= 0 : return
        self.tlog.SetInsertionPoint(pos)
        self.tlog.SetStyle(pos, size, wx.TextAttr("black", "turquoise"))
        self.oldpos  = pos
        self.wordlen = size

    def unhighlightText(self, pos, size):
        if pos<0     : return
        if size <= 0 : return
        self.tlog.SetInsertionPoint(pos)
        self.tlog.SetStyle(pos, size, wx.TextAttr("black", "white"))
        self.oldpos  = -1
        self.wordlen =  0

    def find_str(self,substr, text, poscur, sens):
        pos_founds =  [m.start() for m in re.finditer(substr, text)]
        if len( pos_founds ) == 0 :
            return -1
        if sens < 0 :
            pos_founds.reverse()
            for pos in pos_founds :
                if  pos < poscur :
                    return pos
            return pos_founds[0]
        else:
            for pos in pos_founds :
                if pos > poscur  :
                    return pos
            return pos_founds[0]

    def SetStepNumber(self, value):
        self.stepNumber = max( 1,int( value) )

    def SetFinishBar(self):
        if self.progressBar is None : return
        self.progressBar.SetValue(100)
        wx.SafeYield()

    def SetProgressBar(self,value):
        if self.progressBar is None : return
        newvalue = min(100, int( abs(value) / self.stepNumber * 100.0 +0.5 ))
        precedent=self.progressBar.GetValue()
        if precedent!= newvalue :
            self.progressBar.SetValue(newvalue)
            wx.SafeYield()

    def LogTraceMain(self, enable , homedir= "" ) :
        self.fdlogMain= self.open_logfile(self.fdlogMain, enable , homedir,'solarcan.log' )

    def LogTraceImage(self, enable , videoname="" ) :
        imagedir=""
        imglog_filename=""
        if enable == True and  os.path.exists(videoname):
            imagedir=os.path.dirname(videoname)
            racine=os.path.splitext(os.path.basename(videoname))[0]
            imagedir=os.path.join(imagedir,constantes.PREFIXDIR+racine)
            imglog_filename=racine+".log"
        self.fdlogImg= self.open_logfile(self.fdlogImg, enable, imagedir, imglog_filename )

    def close_logfile(self,fd):
        if fd is not None :
            try:
                fd.close()
            except: # pylint: disable=bare-except
                pass

    def open_logfile(self, fd, enable , logdirname, logfilename ) :
        if enable or len(logfilename)!=0 :
            if fd is not None :
                self.close_logfile(fd)
            try:
                tools.mkdirs(logdirname)
                logfilename = os.path.join(logdirname,logfilename )
                fd  = open(logfilename,"w", encoding="utf-8")
            except Exception as e : # pylint: disable=broad-except
                print("*** Error open logfile :", logfilename+"\n")
                print("*** LogConsole::LogTrace() error :" + str(e), flush=True )
                fd = None
        else:
            self.close_logfile(fd)
            fd=None
        return fd

    def text(self,*args):
        self.insert(*args)
        self.insert("\n")

    def insert(self,*args):
        for arg in args:
            self.logProcess(str(arg)+'\r' )
        self.update()

    #def addProgress(self,texte):
    #    if self.fileRxProg.empty():
    #        self.fileRxProg.put(texte )

    def ligne(self,caract):
        self.insert(caract*80 +'\n')

    def titre(self,caract,*args):
        titre=""
        for arg in args:
            titre=titre+str(arg)
        length = int( (80 - (len(titre)+2*len(caract)) )/2 )
        length = max(0,length)
        if int(len(titre)) & 1 == 1 :    titre += ' '
        self.insert(caract + ' '*length + titre + ' '*length +caract + '\n')

    def update(self):
        if self.fdlogMain is not None: self.fdlogMain.flush()
        if self.fdlogImg is not None: self.fdlogImg.flush()
        #self.tlog.Update()
        #self.tlog.Refresh()
        if self.tlog is not None : wx.SafeYield()

    def clear(self):
        self.tlog.Clear()
        self.SetProgressBar(0)
        self.stepNumber   = 1

    def SetStatus(self, txt=" "):
        pass

    def AbortMsg(self, txt="... Aborted ..."):
        self.ligne('.')
        self.insert('\n' + txt + '\n')
        self.ligne('.')
        self.SetProgressBar(0)
        self.stepNumber   = 1

    def logProcess(self, buffer ):
        try:
            if buffer != "" :
                buffer = buffer.translate(self.translation) # suppression des accents du log
                lines=buffer.split('\r')
                for line in lines :
                    if (len(line)==0) :
                        continue
                    if (line[0] == "#") :
                        num_style = 1
                    elif (line[0] == ".") or  (line[0] == "-") :
                        num_style = 2
                    elif line[0] == "*" :
                        num_style = 3
                    else:
                        num_style = 0
                    if self.tlog is not None :
                        self.tlog.SetDefaultStyle(self.style[num_style])
                        self.tlog.AppendText(line)
                    else:
                        print(line,end='',flush=True)
                    if self.fdlogMain is not None : self.fdlogMain.write(line)
                    if self.fdlogImg is not None : self.fdlogImg.write(line)

        except Exception as e : # pylint: disable=broad-except
            print(" abort LogConsole::logProcess : " +str(e) + '\n', flush=True )

    def textProgProcess(self, buffer):
        try:
            if  buffer != "" :
                self.SetStatus(buffer)
        except Exception as e : # pylint: disable=broad-except
            print(" abort LogConsole::textProgProcess : " +str(e) + '\n', flush=True )
