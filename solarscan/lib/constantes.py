#! /usr/bin/env python

# -*- coding: UTF-8 -*-

# ------------------------------------------------------------------------------
# Project: solarscan ( Solar Scan processing )
#
# This script processes the video solar acquisition of the sun made with the
# instrument Sol'Ex (http://www.astrosurf.com/solex/).
#
# ------------------------------------------------------------------------------
#    Author:  M27trognondepomme <pebe92 (at) gmail.com>
#
# This program is provided without any guarantee.
#
# The license is  LGPL-v3
# For details, see GNU General Public License, version 3 or later.
#                        "https://www.gnu.org/licenses/gpl.html"
# ------------------------------------------------------------------------------

__all__ = [
           "TAB_PROCESSUS", "TAB_FILES", "TAB_LOG",
           "BGCOLOR1", "FGCOLOR1", "WBORDER"
            ]

TAB_PROCESSUS=0
TAB_FILES    =1
TAB_LOG      =3

PREFIXDIR="solarscan_"


BGCOLOR1="#708090" # SlateGray
FGCOLOR1="#FFFAFA" # Snow
WBORDER=2

